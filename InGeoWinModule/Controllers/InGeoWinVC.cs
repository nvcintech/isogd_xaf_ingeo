﻿using System;
using System.Linq;
using System.Text;
using DevExpress.ExpressApp;
using DevExpress.Data.Filtering;
using System.Collections.Generic;
using DevExpress.Persistent.Base;
using DevExpress.ExpressApp.Utils;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Templates;
using DevExpress.Persistent.Validation;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Model.NodeGenerators;
using System.Collections;
using System.Runtime.InteropServices;
using Ingeo;
using DevExpress.Persistent.BaseImpl;
using DevExpress.XtraEditors;
using System.Windows.Forms;
using InGeoModule.InGeoSettings;

namespace InGeoWinModule.Controllers
{
    // For more typical usage scenarios, be sure to check out http://documentation.devexpress.com/#Xaf/clsDevExpressExpressAppViewControllertopic.
    public partial class InGeoWinVC : ViewController
    {
        public InGeoWinVC()
        {
            InitializeComponent();
            RegisterActions(components);
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            IList list = ObjectSpace.GetObjects(typeof(SpatialConfig));
            if (list.Cast<SpatialConfig>().FirstOrDefault(mc => mc.ClassName == View.ObjectTypeInfo.Type.Name) != null)
            {
                Frame.GetController<InGeoWinVC>().SetInGeoLink.Active.SetItemValue("myReason", true);
                Frame.GetController<InGeoWinVC>().ViewInGeoLink.Active.SetItemValue("myReason", true);
            }
            else
            {
                Frame.GetController<InGeoWinVC>().SetInGeoLink.Active.SetItemValue("myReason", false);
                Frame.GetController<InGeoWinVC>().ViewInGeoLink.Active.SetItemValue("myReason", false);
            }
            // Perform various tasks depending on the target View.
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }

        [DllImport("oleaut32.dll")]
        private static extern long GetActiveObject(ref Guid rclsid, IntPtr pvResreved, out IntPtr ppunk);
        public IngeoApplication GetActiveIngeo()
        {
            IntPtr ppunk;
            var rclsid = new Guid("{04088492-0485-11D4-9719-000021C6D845}");
            GetActiveObject(ref rclsid, IntPtr.Zero, out ppunk);
            if (ppunk.ToInt32() != 0)
                return Marshal.GetObjectForIUnknown(ppunk) as IngeoApplication;
            else
                return null;
        }

        private void CreateIngeoLinkTable(IIngeoApplication ingeo)
        {
            IIngeoSemDbTable semDbTable = ingeo.ActiveDb.SemDbTables.Add("XAF_Ingeo_Link");
            semDbTable.Fields.Add("ID", TIngeoFieldDataType.inftString, 12, 0, 0);
            semDbTable.Fields.Add("IsogdObjectId", TIngeoFieldDataType.inftString, 128,
                TIngeoFieldFlags.inffRequired, 0);
            semDbTable.Fields.Add("IsogdClassId", TIngeoFieldDataType.inftString, 128,
                TIngeoFieldFlags.inffRequired, 0);
            semDbTable.Fields.Add("IngeoObjectId", TIngeoFieldDataType.inftString, 128,
                TIngeoFieldFlags.inffRequired, 0);
            semDbTable.Fields.Add("IngeoLayerId", TIngeoFieldDataType.inftString, 128,
                TIngeoFieldFlags.inffRequired, 0);
            semDbTable.Indexes.Add("ID", "ID", TIngeoIndexFlags.inixPrimary);
            semDbTable.Update();
            ingeo.ActiveDb.DropCache();
        }

        const string SemDbTableName = "XAF_Ingeo_Link";

        private void SetInGeoLink_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            var id = ((BaseObject)e.CurrentObject).Oid;
            var className = ((BaseObject)e.CurrentObject).ClassInfo.TableName;
            IIngeoApplication ingeo = GetActiveIngeo();
            if (ingeo == null)
                XtraMessageBox.Show("Не запущено Ингео.");
            else
            {
                DialogResult[] dialogResults = { DialogResult.Yes, DialogResult.No };
                if (!ingeo.ActiveDb.SemDbTables.Exists(SemDbTableName))
                {
                    var xtraMessageBoxForm = new XtraMessageBoxForm();
                    if (
                        xtraMessageBoxForm.ShowMessageBoxDialog(new XtraMessageBoxArgs(null,
                            "Таблица связи отсутствует! Создать?",
                            "Таблица связи отсутствует", dialogResults, null, 0)) == DialogResult.Yes)
                    {
                        CreateIngeoLinkTable(ingeo);
                    }
                    else
                    {
                        return;
                    }
                }

                IIngeoSemDbTable dbTable = ingeo.ActiveDb.SemDbTables[SemDbTableName];
                if (dbTable.Fields.Count != 5 ||
                    dbTable.Fields.Cast<IIngeoSemDbField>().FirstOrDefault(mc => mc.Name == "ID") == null ||
                    dbTable.Fields.Cast<IIngeoSemDbField>().FirstOrDefault(mc => mc.Name == "IsogdObjectId") == null ||
                    dbTable.Fields.Cast<IIngeoSemDbField>().FirstOrDefault(mc => mc.Name == "IsogdClassId") == null ||
                    dbTable.Fields.Cast<IIngeoSemDbField>().FirstOrDefault(mc => mc.Name == "IngeoObjectId") == null ||
                    dbTable.Fields.Cast<IIngeoSemDbField>().FirstOrDefault(mc => mc.Name == "IngeoLayerId") == null)
                {
                    var xtraMessageBoxForm = new XtraMessageBoxForm();
                    if (
                        xtraMessageBoxForm.ShowMessageBoxDialog(new XtraMessageBoxArgs(null,
                            "Ошибка структуры таблицы. Пересоздать?" + Environment.NewLine +
                            "Таблица будет удалена!",
                            "Ошибка структуры таблицы", dialogResults, null, 0)) == DialogResult.Yes)
                    {
                        dbTable.Delete();
                        dbTable.Update();
                        ingeo.ActiveDb.DropCache();
                        CreateIngeoLinkTable(ingeo);
                    }
                    else
                    {
                        return;
                    }

                }
                else
                {
                    IList list = ObjectSpace.GetObjects(typeof(SpatialConfig));
                    if (ingeo.Selection.Count == 0)
                    {
                        XtraMessageBox.Show("Нет выделенного объкта на карте");
                    }
                    for (int i = 0; i < ingeo.Selection.Count; i++)
                    {
                        var ingeoId = ingeo.Selection.IDs[i];
                        object space, number;
                        ingeo.ActiveDb.LocalIDToGlobalID(ingeo.ActiveDb.MapObjects.GetObject(ingeoId).LayerID, out space,
                            out number);
                        var ingeoLayer = space.ToString() + number;
                        if (
                            list.Cast<SpatialConfig>()
                                .FirstOrDefault(mc => mc.ClassName == className && mc.LayerId == ingeoLayer) == null)
                        {
                            XtraMessageBox.Show("Данный класс нельзя связать с выделенным ГИС объектом. Проверьте настройки связи."
                                , "Ошибка");
                            continue;
                        }
                        IIngeoSemDbDataSet ds = dbTable.SelectData("IngeoObjectId", "IngeoObjectId = '" + ingeoId + "'",
                            null);
                        if (!ds.EOF)
                        {
                            var xtraMessageBoxForm = new XtraMessageBoxForm();
                            if (
                                xtraMessageBoxForm.ShowMessageBoxDialog(new XtraMessageBoxArgs(null,
                                    "Данный объект уже связан. Пересвязать?",
                                    "Данный объект уже связан", dialogResults, null, 0)) == DialogResult.Yes)
                            {
                                dbTable.DeleteData("IngeoObjectId=?", new object[] { ingeoId });

                                dbTable.InsertData("ID, IsogdObjectId, IsogdClassId, IngeoObjectId, IngeoLayerId",
                                    "?,?,?,?,?",
                                    new[]
                                    {
                                        ingeo.ActiveDb.GenerateID(), id.ToString(), className, ingeoId, ingeoLayer
                                    });
                            }
                            else
                            {
                                return;
                            }
                        }
                        else
                        {
                            dbTable.InsertData("ID, IsogdObjectId, IsogdClassId, IngeoObjectId, IngeoLayerId",
                                "?,?,?,?,?",
                                new[]
                                {
                                    ingeo.ActiveDb.GenerateID(), id.ToString(), className, ingeoId, ingeoLayer
                                });
                        }
                    }
                }

            }
        }

        private void ViewInGeoLink_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            var id = ((BaseObject)e.CurrentObject).Oid;
            var className = ((BaseObject)e.CurrentObject).ClassInfo.TableName;
            var ingeo = GetActiveIngeo();
            if (ingeo == null)
            {
                XtraMessageBox.Show("InGeo не запущено", "Ошибка");
                return;
            }
            if (!ingeo.ActiveDb.SemDbTables.Exists(SemDbTableName))
            {
                XtraMessageBox.Show("Таблица не найдена", "Ошибка");
                return;
            }
            var dbTable = ingeo.ActiveDb.SemDbTables[SemDbTableName];

            var dataSet = dbTable.SelectData("IngeoObjectId",
                "IsogdObjectId=? and IsogdClassId=?", new[] { id.ToString(), className });
            if (dataSet.EOF)
            {
                XtraMessageBox.Show("Объект не связан");
                return;
            }
            ingeo.Selection.DeselectAll();
            double x1 = 0, x2 = 0, y1 = 0, y2 = 0;
            while (!dataSet.EOF)
            {
                var ingeoObejctId = (string)dataSet.Fields[0].Value;
                IIngeoMapObject mapObject = null;
                try
                {
                    mapObject = ingeo.ActiveDb.MapObjects.GetObject(ingeoObejctId);
                }
                catch
                {
                }
                if (mapObject != null)
                {
                    if (x1 == 0 && x2 == 0 && y1 == 0 && y2 == 0)
                    {
                        x1 = mapObject.X1;
                        x2 = mapObject.X2;
                        y1 = mapObject.Y1;
                        y2 = mapObject.Y2;
                        ingeo.Selection.Select(ingeoObejctId, 0);
                    }
                    else
                    {
                        if (x1 > mapObject.X1) x1 = mapObject.X1;
                        if (x2 < mapObject.X2) x2 = mapObject.X2;
                        if (y1 > mapObject.Y1) y1 = mapObject.Y1;
                        if (y2 < mapObject.Y2) y2 = mapObject.Y2;
                        ingeo.Selection.Select(ingeoObejctId, 0);
                    }
                }
                else
                {
                    dbTable.DeleteData("IngeoObjectId=?", new object[] { ingeoObejctId });
                }
                dataSet.MoveNext();
            }
            if (x1 != 0 && x2 != 0 && y1 != 0 && y2 != 0)
            {
                ingeo.MainWindow.MapWindow.Navigator.FitWorldBounds(x1, y1, x2, y2,
                    TIngeoNavigatorFitMode.infitAlwaysScale);

                System.Diagnostics.Process[] p = System.Diagnostics.Process.GetProcessesByName("InGeo");
                if (p.Length > 0)
                {
                    ShowWindow(p[0].MainWindowHandle, 10);
                    ShowWindow(p[0].MainWindowHandle, 5);
                    SetForegroundWindow(p[0].MainWindowHandle);
                }
            }
            dataSet.Close();
        }
        [DllImport("user32.dll")]
        private static extern bool ShowWindow(IntPtr handle, int cmdShow);
        [DllImport("user32.dll")]
        private static extern int SetForegroundWindow(IntPtr handle);
    }
}
