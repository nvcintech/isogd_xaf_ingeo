﻿namespace InGeoWinModule.Controllers
{
    partial class InGeoWinVC
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.SetInGeoLink = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.ViewInGeoLink = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // SetInGeoLink
            // 
            this.SetInGeoLink.Caption = "Связать";
            this.SetInGeoLink.ConfirmationMessage = null;
            this.SetInGeoLink.Id = "SetInGeoLink";
            this.SetInGeoLink.ToolTip = "Связать с выделенным объктом на карте";
            this.SetInGeoLink.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.SetInGeoLink_Execute);
            // 
            // ViewInGeoLink
            // 
            this.ViewInGeoLink.Caption = "Показать";
            this.ViewInGeoLink.ConfirmationMessage = null;
            this.ViewInGeoLink.Id = "ViewInGeoLink";
            this.ViewInGeoLink.ToolTip = "Показать связанный объкт на карте";
            this.ViewInGeoLink.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.ViewInGeoLink_Execute);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction SetInGeoLink;
        private DevExpress.ExpressApp.Actions.SimpleAction ViewInGeoLink;
    }
}
