﻿using System;
using DevExpress.Xpo;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Base;

namespace InGeoModule.InGeoSettings
{
    [Custom("Caption", "Настройки ИнГео"), NavigationItem("Настройки Ингео")]
    public class SpatialConfig : BaseObject
    {
        public SpatialConfig(): base(){}
        public SpatialConfig(Session session) : base(session) {}
        private string _layerId;
        private string _className;

        [DisplayName("Наименование класса")]
        public string ClassName
        {
            get { return _className; }
            set { SetPropertyValue("ClassName", ref _className, value); }
        }

        [DisplayName("ID слоя Ингео")]
        public string LayerId
        {
            get { return _layerId; }
            set { SetPropertyValue("LayerId", ref _layerId, value); }
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
        }
    }

}