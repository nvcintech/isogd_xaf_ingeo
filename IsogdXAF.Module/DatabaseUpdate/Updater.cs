using System;
using System.Linq;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp.Xpo;
using DevExpress.Persistent.Base;
using DevExpress.ExpressApp.Updating;
using DevExpress.Persistent.BaseImpl;
using DevExpress.ExpressApp.Security;
using DevExpress.ExpressApp.Security.Strategy;
using ISOGDXAF.OrgStructure;
using ISOGDXAF.Isogd;
using ISOGDXAF.Enums;
//using DevExpress.ExpressApp.Reports;
//using DevExpress.ExpressApp.PivotChart;
//using DevExpress.ExpressApp.Security.Strategy;
//using IsogdXAF.Module.BusinessObjects;

namespace IsogdXAF.Module.DatabaseUpdate
{
    // For more typical usage scenarios, be sure to check out http://documentation.devexpress.com/#Xaf/clsDevExpressExpressAppUpdatingModuleUpdatertopic
    public class Updater : ModuleUpdater
    {
        public Updater(IObjectSpace objectSpace, Version currentDBVersion) :
            base(objectSpace, currentDBVersion)
        {
        }
        public override void UpdateDatabaseAfterUpdateSchema()
        {
            base.UpdateDatabaseAfterUpdateSchema();
            SecuritySystemRole adminRole = ObjectSpace.FindObject<SecuritySystemRole>(
            new BinaryOperator("Name", "�������������"));
            if (adminRole == null)
            {
                adminRole = ObjectSpace.CreateObject<SecuritySystemRole>();
                adminRole.Name = "�������������";
                adminRole.IsAdministrative = true;
            }
            adminRole.CanEditModel = true;
            adminRole.Save();

            SecuritySystemUser user1 = ObjectSpace.FindObject<SecuritySystemUser>(
        new BinaryOperator("UserName", "admin"));
            if (user1 == null)
            {
                user1 = ObjectSpace.CreateObject<SecuritySystemUser>();
                user1.UserName = "admin";
                // Set a password if the standard authentication type is used. 
                user1.SetPassword("");

            }
            user1.Roles.Add(adminRole);
            user1.Save();
            
            // ���� �������������� �����������
            string[] munKind = { "0;������� ���������", 
                                   "1;��������� �����", 
                                    "2;������������� �����", 
                                    "3;��������� ���������",
                                    "4;�������� ���������"};
            foreach (string mk in munKind)
                SetMunKind(mk);
            // ������������� �����������
            Municipality mun = ObjectSpace.FindObject<Municipality>(
                new BinaryOperator("Name", "�-��"));
            if (mun == null)
            {
                mun = ObjectSpace.CreateObject<Municipality>();
                mun.Name = "�-��";
                mun.OKTMO = "123456789";
                mun.MunicipalityKind = ObjectSpace.FindObject<MunicipalityKind>(
                new BinaryOperator("Name", "��������� �����"));
            }
            mun.Save();

            // ������
            MainDepartment mainDep = ObjectSpace.FindObject<MainDepartment>(
       new BinaryOperator("Name", "������������� �. �-���"));
            if (mainDep == null)
            {
                mainDep = ObjectSpace.CreateObject<MainDepartment>();
                mainDep.Name = "������������� �. �-���";
                mainDep.Municipality = ObjectSpace.FindObject<Municipality>(
                new BinaryOperator("Name", "�-��"));
            }
            mainDep.Save();

            // ������
            Departament depIsogd = ObjectSpace.FindObject<Departament>(
       new BinaryOperator("BriefName", "�����"));
            if (depIsogd == null)
            {
                depIsogd = ObjectSpace.CreateObject<Departament>();
                depIsogd.BriefName = "�����";
                depIsogd.Name = "����� �����";
                depIsogd.MainDepartment = mainDep;
            }
            depIsogd.Save();

            // ����������
            Employee empl = ObjectSpace.FindObject<Employee>(
       new BinaryOperator("UserName", "������ �. �."));
            if (empl == null)
            {
                empl = ObjectSpace.CreateObject<Employee>();
                empl.UserName = "������ �. �.";
                empl.Position = "��������� ������";
                empl.SysUser = user1;
                empl.FirstName = "����";
                empl.MiddleName = "��������";
                empl.LastName = "������";
                empl.FullName = "������ ���� ��������";
                empl.Departament = depIsogd;
            }
            //dep.Roles.Add(adminRole);
            empl.Save();

            string[] docKindName = { "����������������� ���� ���������� �������", "������������� �� ����������� ������������������ �����", 
                                       "���������� �� �������������", "���������� �� ���� ������� � ������������",
                                       "�������� �� ���"};
            foreach (string dk in docKindName)
                SetDocKind(dk);

            string[] isogdPart = {"01;��������� ���������������� ������������ ��;��������� ���������������� ������������ ���������� ��������� � �����, ���������� ���������� �������������� �����������",
             "02;��������� ���������������� ������������ �������� ��;��������� ���������������� ������������ �������� ���������� ��������� � �����, ���������� ���������� �������������� �����������", 
              "03;��������� ���������������� ������������;��������� ���������������� ������������ �������������� �����������, ��������� �� �� �����������",
              "04;������� ���������������� � ���������;������� ���������������� � ���������, �������� � ��� ���������",
              "05;������������ �� ���������� ����������;������������ �� ���������� ����������",
               "06;��������� � ����������� �������;����������� ��������� � ����������� ������� �� ��������� ����������� ���������� ���������",
               "07;������� � �������������� ��������� ��������;������� � �������������� ��������� �������� ��� ��������������� ��� ������������� ����",
                 "08;����������� � ���������� ��������� ��������� �������;����������� � ���������� ��������� ��������� �������",
                "09;������������� � ���������������� ���������;������������� � ���������������� ���������"};
            foreach (string ip in isogdPart)
                FillIsogdPartition(ip);

            // ����� ��������� �����
            string[] isogdClass = {"010100;����� �� ���������� ��������� � �����, ���������� ���������� ��;01;0",
             "010200;����� �� ��� � �����, ���������� ���������� ��;02;0", 
              "010300;����� �� ��, ��������� �� �� �����������;03;0",
              "010400;����������� ����� ��������� � ��������� �������;03;0",
              "010500;��������� � ����������� ����;03;0",
               "020100;����������� �������� ��� �� ����������� ������ ���������������� � ���������;04;1",
               "020200;������� ���������� ������ ���������������� � ��������� � ��������  � ��� ��������� (��������� ��������);04;1",
                "020300;����� ������������������ �����������;04;1",
                "020400;����������������� ���������� (��������� ��������);04;1",
                "020500;��������� � ������� ���������������� � ���������;04;1",
                "020600;���������� � ����������� ��������� �������� �� �������� ���������������� � ���������;04;1",
                "030100;������ ���������� ����������;05;2",
                "030200;������ ��������� ����������;05;2",
                "040100;��������� �� ����������� ��������� � ����������� ������� �� ��������� ����������� ���������� ���������;06;3",
                "040200;��������� � ��������� � ����������� ��������(��);06;3",
                "050100;�������� �� �������  ���������� ������� ��� ��������������� ��� ������������� ����;07;4",
                "050200;�������� � �������������� ���������� ������� ��� ��������������� ��� ������������� ����;07;4",
                "060100;����������������� ���� ���������� �������;08;5",
                "060200;����� � ����������� ���������� ���������;08;5",
                "060300;�������� �� ���, ���� � ��;08;5",
                "060400;���������, �������������� ������������ �� ����������� ����������� ����������� � ����������� ���������� ���������;08;5",
                "060500;���������� ��������������� ���������� ��;08;5",
                "060600;���������� �� �������������;08;5",
                "060700;���������� �� ���������� �� ���������� ���������� ������������ �������������, ������������� ���;08;5",
                "060800;������� ���� � �������������� ���������� �� �������-����������� ��� �������������;08;5",
                "060900;���������, �������������� ������������ ������������, �������������������, ������������������ ��� ��������� ������������;08;5",
                "061000;��� ������� ���;08;5",
                "061100;���������� �� ���� ������� � ������������;08;5",
                "061200;�����. ������������ ������������ ���, ������������ ���� � �������� �� � ������������� ����������� ��;08;5",
                "061300;����������� �������� �� ���;08;5",
                                  };
            foreach (string icl in isogdClass)
                FillIsogdClass(icl);
            // � ������� ���� ������� ������ �� �������� ����

            IsogdBooksCard isogdbookscard = ObjectSpace.FindObject<IsogdBooksCard>(CriteriaOperator.Parse(string.Format("IsogdBooksKind = '{0}' and ActiveTomeFlag= 'true'", IsogdBooksKind.���������������������������)));
            if (isogdbookscard == null)
            {
                isogdbookscard = ObjectSpace.CreateObject<IsogdBooksCard>();
                isogdbookscard.IsogdBooksKind = IsogdBooksKind.���������������������������;
                isogdbookscard.BookTomeNo = "1";
                isogdbookscard.ActiveTomeFlag = true;
                isogdbookscard.OpenenigTomeDate = DateTime.Now.Date;
                isogdbookscard.RecordDate = DateTime.Now.Date;
                isogdbookscard.Save();
                ObjectSpace.CommitChanges();
            }
            isogdbookscard = null;
            isogdbookscard = ObjectSpace.FindObject<IsogdBooksCard>(CriteriaOperator.Parse(string.Format("IsogdBooksKind = '{0}' and ActiveTomeFlag= 'true'", IsogdBooksKind.����������������)));
            if (isogdbookscard == null)
            {
                isogdbookscard = ObjectSpace.CreateObject<IsogdBooksCard>();
                isogdbookscard.IsogdBooksKind = IsogdBooksKind.����������������;
                isogdbookscard.BookTomeNo = "1";
                isogdbookscard.ActiveTomeFlag = true;
                isogdbookscard.OpenenigTomeDate = DateTime.Now.Date;
                isogdbookscard.RecordDate = DateTime.Now.Date;
                isogdbookscard.Save();
                ObjectSpace.CommitChanges();
            }
            isogdbookscard = null;
            isogdbookscard = ObjectSpace.FindObject<IsogdBooksCard>(CriteriaOperator.Parse(string.Format("IsogdBooksKind = '{0}' and ActiveTomeFlag= 'true'",IsogdBooksKind.����������������)));
            if (isogdbookscard == null)
            {
                isogdbookscard = ObjectSpace.CreateObject<IsogdBooksCard>();
                isogdbookscard.IsogdBooksKind = IsogdBooksKind.����������������;
                isogdbookscard.BookTomeNo = "1";
                isogdbookscard.ActiveTomeFlag = true;
                isogdbookscard.OpenenigTomeDate = DateTime.Now.Date;
                isogdbookscard.RecordDate = DateTime.Now.Date;
                isogdbookscard.Save();
                ObjectSpace.CommitChanges();
            }
            isogdbookscard = null;
            isogdbookscard = ObjectSpace.FindObject<IsogdBooksCard>(CriteriaOperator.Parse(string.Format("IsogdBooksKind = '{0}' and ActiveTomeFlag= 'true'", IsogdBooksKind.������������������)));
            if (isogdbookscard == null)
            {
                isogdbookscard = ObjectSpace.CreateObject<IsogdBooksCard>();
                isogdbookscard.IsogdBooksKind = IsogdBooksKind.������������������;
                isogdbookscard.BookTomeNo = "1";
                isogdbookscard.ActiveTomeFlag = true;
                isogdbookscard.OpenenigTomeDate = DateTime.Now.Date;
                isogdbookscard.RecordDate = DateTime.Now.Date;
                isogdbookscard.Save();
                ObjectSpace.CommitChanges();
            }
            isogdbookscard = null;

            // ����� ��������
            // ���, ������������, ������, �����
            string[] isogdStorBook = {"01;����� ���������������� ������������ ���������� ���������. ��������� � ��������������� ������������ ���������� ���������;01",
             "01-��;��������� � ���������� ���������������� ������������ ���������� ���������;01", 
              "02;����� ���������������� ������������ �������� ���������� ���������. ��������� � ��������������� ������������ �������� ���������� ���������;02",
              "02-��;��������� � ���������� ���������������� ������������ �������� ���������� ���������;02",
              "03;����������� ���� ��;03",
               "03;����������� ���� ���������;03",
               "03;����� ���������������� ������������ ��;03",
                "03-��;��������� � ���������� ���������������� ������������ ������������� �����������;03",
                "04;������� ���������������� � ��������� ��������� (��);04",
                "04;������� ���������������� � ��������� �� (����������� ����������);04",
                "04-��;��������� � ���������� ������ ���������������� � ���������;04",
                "05;������ ��������� ���������� (��������� ��������);05",
                "05;������ ���������� ����������;05",
                "06-1;���������-�������������;06",
                "06-10;�������� ��������� ������������ ����������;06",
                "06-11;��������� ������������ ����������� ������� � ��������� ���;06",
                "06-2;���������-�������������;06",
                "06-3;���������-����������������������;06",
                "06-4;���������-�������������;06",
                "06-5;���������-��������������;06",
                "06-6;�������������� ������������;06",
                "06-7;������������ ��������� ������� ��������� ������ � ����������, �� ������������ �����������;06",
                "06-8;����� � �������� ��������� ��� ��� ����� �������������;06",
                "06-9;��������� ���������� ����������� ���������� �����;06",
                "07-��;������� ��������� �������� ��� ��������������� � ������������� ����;07",
                "07-��;�������������� ��������� �������� ��� ��������������� � ������������� ����;07",
                                  };
            foreach (string isb in isogdStorBook)
                FillIsogdStorBooks(isb);


            // ����� ������������� ��������� �����
            string[] isogdDocForm = { "1;�������� ��������", 
                                      "1.00;������ ������ �0", 
                                      "1.01;������ ������ �1", 
                                    "1.02;������ ������ �2", 
                                    "1.03;������ ������ �3", 
                                    "1.04;������ ������ �4", 
                                    "1.05;������ ������ �5", 
                                    "2;����������� ��������", 
                                    "2.01;��������� ����� ������������� ���������", 
                                    "2.02;��������� ����� ������������� ���������", };
            foreach (string idf in isogdDocForm)
                fillIsogdDocForm(idf);
            ObjectSpace.CommitChanges();
        }
        public override void UpdateDatabaseBeforeUpdateSchema()
        {
            base.UpdateDatabaseBeforeUpdateSchema();
            //if(CurrentDBVersion < new Version("1.1.0.0") && CurrentDBVersion > new Version("0.0.0.0")) {
            //    RenameColumn("DomainObject1Table", "OldColumnName", "NewColumnName");
            //}
        }

        private void SetMunKind(string munkind)
        {
            string munkindcode = munkind.Split(';')[0];
            string munkindname = munkind.Split(';')[1];
            MunicipalityKind municipalityKind = ObjectSpace.FindObject<MunicipalityKind>(new BinaryOperator("Name", munkindname));
            if (municipalityKind == null)
            {
                municipalityKind = ObjectSpace.CreateObject<MunicipalityKind>();
                municipalityKind.Code = munkindcode;
                municipalityKind.Name = munkindname;
            }
            municipalityKind.Save();
            ObjectSpace.CommitChanges();
            municipalityKind = null;
        }

        private void SetDocKind(string name)
        {
            IsogdDocumentNameKind isogddocKind = ObjectSpace.FindObject<IsogdDocumentNameKind>(new BinaryOperator("Name", name));
            if (isogddocKind == null)
            {
                isogddocKind = ObjectSpace.CreateObject<IsogdDocumentNameKind>();
                isogddocKind.Name = name;
            }
            isogddocKind.Save();
            ObjectSpace.CommitChanges();
            isogddocKind = null;
        }
        private void FillIsogdPartition(string isogdpartText)
        {
            string partno = isogdpartText.Split(';')[0];
            string name = isogdpartText.Split(';')[1];
            string fullname = isogdpartText.Split(';')[2];
            IsogdPartition isogdpart = ObjectSpace.FindObject<IsogdPartition>(new BinaryOperator("Name", name));
            if (isogdpart == null)
            {
                isogdpart = ObjectSpace.CreateObject<IsogdPartition>();
                isogdpart.PartNo = partno;
                isogdpart.Name = name;
                isogdpart.FullName = fullname;
                isogdpart.PartitionType = 0;
            }
            isogdpart.Save();
            ObjectSpace.CommitChanges();
            isogdpart = null;
        }
        private void FillIsogdClass(string isogdclassText)
        {
            string code = isogdclassText.Split(';')[0];
            string name = isogdclassText.Split(';')[1];
            string part = isogdclassText.Split(';')[2];
            int mainClass = Convert.ToInt32(isogdclassText.Split(';')[3]);
            IsogdDocumentClass isogdDocClass = ObjectSpace.FindObject<IsogdDocumentClass>(new BinaryOperator("Name", name));
            if (isogdDocClass == null)
            {
                isogdDocClass = ObjectSpace.CreateObject<IsogdDocumentClass>();
                isogdDocClass.Name = name;
                isogdDocClass.Code = code;
                isogdDocClass.IsogdPartition = ObjectSpace.FindObject<IsogdPartition>(new BinaryOperator("PartNo", part));
                isogdDocClass.IsogdDocumentMainClass = (IsogdDocumentMainClass)mainClass;
            }
            isogdDocClass.Save();
            ObjectSpace.CommitChanges();
            isogdDocClass = null;
        }
        private void FillIsogdStorBooks(string isogdstbookText)
        {
            string code = isogdstbookText.Split(';')[0];
            string name = isogdstbookText.Split(';')[1];
            string part = isogdstbookText.Split(';')[2];
            //string iclass = isogdstbookText.Split(';')[3];
            IsogdStorageBook isogdStorageBook = ObjectSpace.FindObject<IsogdStorageBook>(new BinaryOperator("Name", name));
            if (isogdStorageBook == null)
            {
                //������� ����� ��������
                isogdStorageBook = ObjectSpace.CreateObject<IsogdStorageBook>();
                isogdStorageBook.Name = name;
                isogdStorageBook.Code = code;
                isogdStorageBook.IsogdPartition = ObjectSpace.FindObject<IsogdPartition>(new BinaryOperator("PartNo", part));
                //// ������� ����� ��������� ����� �� ����, � ����������� � ���� ������ �� ��������� ����� ��������
                //IsogdDocumentClass isogdDocumentClass = ObjectSpace.FindObject<IsogdDocumentClass>(new BinaryOperator("Code", iclass));
                //if (isogdDocumentClass != null)
                //{
                //    isogdDocumentClass.IsogdStorageBook = isogdStorageBook;
                //    isogdDocumentClass.Save();
                //}
                // ������� ��� ����� ��������
                IsogdStorageBookTome isogdstorebooktome = ObjectSpace.CreateObject<IsogdStorageBookTome>();
                isogdstorebooktome.TomeNo = "1";
                isogdstorebooktome.Name = String.Format("��� � 1 ����� {0} ({1})", code, name);
                isogdstorebooktome.OpenenigTomeDate = DateTime.Now.Date;
                isogdstorebooktome.ActiveTomeFlag = true;
                isogdstorebooktome.Save();
                isogdStorageBook.IsogdStorageBookTome.Add(isogdstorebooktome);
                // � ������� ���� ������� ������ �� �������� ����
                IsogdBooksCard isogdbookscard = ObjectSpace.CreateObject<IsogdBooksCard>();
                isogdbookscard.IsogdBooksKind = IsogdBooksKind.�������������;
                isogdbookscard.BookTomeNo = "1";
                isogdbookscard.ActiveTomeFlag = true;
                isogdbookscard.OpenenigTomeDate = DateTime.Now.Date;
                isogdbookscard.RecordDate = DateTime.Now.Date;
                isogdbookscard.IsogdStorageBook = isogdStorageBook;
                isogdbookscard.Save();
            }
            isogdStorageBook.Save();
            ObjectSpace.CommitChanges();
            isogdStorageBook = null;
        }

        private void fillIsogdDocForm(string codename)
        {
            string code = codename.Split(';')[0];
            string name = codename.Split(';')[1];
            IsogdDocumentForm isogdDocForm = ObjectSpace.FindObject<IsogdDocumentForm>(new BinaryOperator("Code", code));
            if (isogdDocForm == null)
            {
                isogdDocForm = ObjectSpace.CreateObject<IsogdDocumentForm>();
                isogdDocForm.Code = code;
                isogdDocForm.Name = name;
            }
            isogdDocForm.Save();
            ObjectSpace.CommitChanges();
            isogdDocForm = null;
        }
    }
}
