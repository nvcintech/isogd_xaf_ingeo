using System;
using DevExpress.Xpo;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Base;

namespace ISOGDXAF.Land
{
    [Custom("Caption", "���������� ���������� �������"), System.ComponentModel.DefaultProperty("Name")]
    public class LotName  : BaseObject
    {
        public LotName(Session session) : base(session) { }
        
        [Size(255), DisplayName("���")]
        public string Code { get; set; }

        [Size(255), DisplayName("����������")]
        public string Name { get; set; }
    }
}