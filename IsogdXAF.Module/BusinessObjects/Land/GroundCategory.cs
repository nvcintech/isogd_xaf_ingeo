using System;
using DevExpress.Xpo;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Base;

namespace ISOGDXAF.Land
{
    /// <summary>
    /// ��������� ������
    /// </summary>
    [Custom("Caption", "��������� ������"), System.ComponentModel.DefaultProperty("Name")]
    public class GroundCategory  : BaseObject
    {
        public GroundCategory(Session session) : base(session) { }
        
        [Size(255), DisplayName("���")]
        public string Code { get; set; }

        [Size(255), DisplayName("������������")]
        public string Name { get; set; }
    }
}