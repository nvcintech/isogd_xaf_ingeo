using System;
using DevExpress.Xpo;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Base;
using ISOGDXAF.Isogd;
using ISOGDXAF.OrgStructure;
using ISOGDXAF.General;

namespace ISOGDXAF.Land
{
    [NavigationItem("����� �������")]
    [Custom("Caption", "��������� �������"), System.ComponentModel.DefaultProperty("descript")]
    public class Lot : AttachBase
    {
        public Lot(Session session) : base(session) { }

        [Size(255), System.ComponentModel.Browsable(false)]
        public string descript
        {
            get
            {
                return String.Format("��������� ������� ʹ {0}, {1}", CadNo, Name);
            }
        }

        [Size(255), DisplayName("����������� �����")]
        public string CadNo { get; set; }

        [DisplayName("���� ���������� �� ����������� ����")]
        public DateTime CadNoDate { get; set; }

        [DisplayName("���� ������ � �����")]
        public DateTime DateRemoved { get; set; }

        [Size(255), DisplayName("���������� ���������� ������� (���������)")]
        [ImmediatePostData]
        public LotName LotName { get; set; }

        private string i_name;
        [Size(510), DisplayName("����������")]
        public string Name
        {
            get
            {
                if (LotName != null)
                    if (LotName.Name.Length > 0)
                        i_name = LotName.Name;
                return i_name;
            }
            set { SetPropertyValue("Name", ref i_name, value); }
        }
        
        [Size(255), DisplayName("�������� ��������")]
        public string Adress { get; set; }

        [DisplayName("������� ���������� ������� (�2.)")]
        public double LotSquare { get; set; }

        [DisplayName("��������� ������")]
        public GroundCategory GroundCategory { get; set; }

        [DisplayName("������������� ���������� �������")]
        public UsingWay UsingWay { get; set; }

        private string i_RightHoldersText;
        [Size(510), DisplayName("���������������")]
        public string RightHoldersText
        {
            get
            {
                string res = String.Empty;
                try
                {
                    foreach (RightHolder rh in RightHolders)
                    {
                        if (res != String.Empty)
                            res += ", ";
                        res += rh.RequesterString;
                    }
                }
                catch { }
                return res;
            }
            set { SetPropertyValue("RightHoldersText", ref i_RightHoldersText, value); }
        }
        private Municipality _Municipality;
        [DisplayName("������������� �����������")]
        public Municipality Municipality
        {
            get { return _Municipality; }
            set { try { SetPropertyValue("Municipality", ref _Municipality, value); } catch { } }
        }
        [Size(SizeAttribute.Unlimited), DisplayName("����������")]
        public string Notes { get; set; }

        [Association, DisplayName("���������������")]
        public XPCollection<RightHolder> RightHolders
        {
            get
            {
                return GetCollection<RightHolder>("RightHolders");
            }
        }

        [Association, DisplayName("��������� �����, ��������� � ��������")]
        //[System.ComponentModel.Browsable(false)]
        public XPCollection<IsogdDocument> IsogdDocument
        {
            get { return GetCollection<IsogdDocument>("IsogdDocument"); }
        }

        [Association, DisplayName("����� �������� �����")]
        public XPCollection<IsogdStorageBook> IsogdStorageBook
        {
            get { return GetCollection<IsogdStorageBook>("IsogdStorageBook"); }
        }
    }
}