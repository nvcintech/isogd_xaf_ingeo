using System;
using DevExpress.Xpo;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Base;
using ISOGDXAF.Dictonaries;
using ISOGDXAF.Enums;
using ISOGDXAF.Subject;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.Editors;

namespace ISOGDXAF.Land
{
    [Custom("Caption", "��������������"), System.ComponentModel.DefaultProperty("RequesterString")]
    public class RightHolder  : BaseObject
    {
        public RightHolder(Session session) : base(session) { }

        private string i_requesterString;
        private Org i_org;
        private ISOGDXAF.Subject.Person i_person;
        private RequesterType i_requesterType;
        private DateTime i_lawStartDate;
        private DateTime i_lawFinishDate;
        private string i_lawDocument;
        private double i_lawProcent;

        [DisplayName("��� ���������������")]
        [ImmediatePostData]
        public RequesterType RequesterType
        {
            get { return i_requesterType; }
            set { SetPropertyValue("RequesterType", ref i_requesterType, value); }
        }
        [DisplayName("���������������")]
        [Appearance("isOrg", Visibility = ViewItemVisibility.Hide, Criteria = "RequesterType= '���'", Context = "DetailView")]
        public Org RequesterOrg
        {
            get { return i_org; }
            set { SetPropertyValue("RequesterOrg", ref i_org, value); }
        }

        [DisplayName("���������������")]
        [Appearance("isPerson", Visibility = ViewItemVisibility.Hide, Criteria = "RequesterType= '��'", Context = "DetailView")]
        public ISOGDXAF.Subject.Person RequesterPerson
        {
            get { return i_person; }
            set { SetPropertyValue("RequesterPerson", ref i_person, value); }
        }

        [Size(510), DisplayName("�������� ���������������")]
        public string RequesterString
        {
            get
            {
                try
                {
                    i_requesterString = GetRequesterInfo();
                }
                catch { }
                return i_requesterString;
            }
            set { SetPropertyValue("RequesterString", ref i_requesterString, value); }
        }

        public string GetRequesterInfo()
        {
            string result = "";
            if (i_org != null)
            {
                result = i_org.FullName;
                if (i_org.Info.Length > 0)
                    result += ", " + i_org.Info;
            }
            if (i_person != null)
            {
                result = i_person.FullName;
                if (i_person.Info.Length > 0)
                    result += ", " + i_person.Info;
            }
            return result;
        }
        [DisplayName("��� �����")]
        public LawKind LawKind { get; set; }

        [DisplayName("���� ������ �������� �����")]
        public DateTime LawStartDate
        {
            get { return i_lawStartDate; }
            set { SetPropertyValue("LawStartDate", ref i_lawStartDate, value); }
        }

        [DisplayName("���� ��������� �������� �����")]
        public DateTime LawFinishDate
        {
            get { return i_lawFinishDate; }
            set { SetPropertyValue("LawFinishDate", ref i_lawFinishDate, value); }
        }

        [Size(510),DisplayName("�������� �������� �����")]
        public string LawDocument
        {
            get { return i_lawDocument; }
            set { SetPropertyValue("LawDocument", ref i_lawDocument, value); }
        }

        [DisplayName("������� ��������������")]
        public double LawProcent
        {
            get { return i_lawProcent; }
            set { SetPropertyValue("LawProcent", ref i_lawProcent, value); }
        }


        [Association, DisplayName("��������� �������")]
        public XPCollection<Lot> Lots
        {
            get
            {
                return GetCollection<Lot>("Lots");
            }
        }
    }
}