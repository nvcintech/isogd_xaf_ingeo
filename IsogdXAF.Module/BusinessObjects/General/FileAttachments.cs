using System;
using DevExpress.Xpo;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Base;
using System.ComponentModel;
using DevExpress.Persistent.Validation;
using ISOGDXAF.OrgStructure;
using DevExpress.ExpressApp;
using DevExpress.Data.Filtering;


namespace ISOGDXAF.General
{
    public class AttachmentFiles : FileAttachmentBase
    {
        public AttachmentFiles(Session session) : base(session) { }
        //private DocumentType documentType;
        protected AttachBase _attachBase;
        
        private string i_Name;
        private DateTime i_RegDate;
        private Employee i_Registrator;
        private string i_Description;

        [Size(255), DevExpress.Xpo.DisplayName("������������")]
        public string Name
        {
            get { return i_Name; }
            set { SetPropertyValue("Name", ref i_Name, value); }
        }
        [DevExpress.Xpo.DisplayName("���� ����������")]
        public DateTime RegDate
        {
            get { return i_RegDate; }
            set { SetPropertyValue("RegDate", ref i_RegDate, value); }
        }
        [DevExpress.Xpo.DisplayName("���������, ������������ ����")]
        public Employee Registrator
        {
            get { return i_Registrator; }
            set { SetPropertyValue("Registrator", ref i_Registrator, value); }
        }
        [Size(4000), DevExpress.Xpo.DisplayName("����������")]
        public string Description
        {
            get { return i_Description; }
            set { SetPropertyValue("Description", ref i_Description, value); }
        }

        [Persistent, Association("AttachBase-AttachmentFiles"), DevExpress.Xpo.DisplayName("����������� �����")]
        [FileTypeFilter("�����", 1, "*.doc", "*.docx", "*.xls", "*.xlsx", "*.txt")]
        [FileTypeFilter("�����������", 2, "*.bmp", "*.png", "*.gif", "*.jpg")]
        public AttachBase AttachBase
        {
            get { return _attachBase; }
            set
            {
                SetPropertyValue("AttachBase", ref _attachBase, value);
            }
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            if (SecuritySystem.CurrentUser != null)
            {
                Employee currentEmpl = Session.FindObject<Employee>(new BinaryOperator("SysUser", SecuritySystem.CurrentUserId));
                if (currentEmpl != null)
                    i_Registrator = currentEmpl;
            }
            i_RegDate = DateTime.Now.Date;
        }
        
    }
}