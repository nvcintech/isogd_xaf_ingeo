using System;
using DevExpress.Xpo;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Base;

namespace ISOGDXAF.General
{

    public class AttachBase : BaseObject
    {
        public AttachBase(Session session) : base(session) { }

        [Aggregated, Association("AttachBase-AttachmentFiles"), DisplayName("����������� �����")]
        [FileTypeFilter("�����", 1, "*.doc", "*.docx", "*.xls", "*.xlsx", "*.txt")]
        [FileTypeFilter("�����������", 2, "*.bmp", "*.png", "*.gif", "*.jpg")]
        public XPCollection<AttachmentFiles> AttachmentFiles
        {
            get { return GetCollection<AttachmentFiles>("AttachmentFiles"); }
        }
     
    }

}