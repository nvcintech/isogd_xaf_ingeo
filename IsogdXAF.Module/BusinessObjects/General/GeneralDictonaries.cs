using System;
using DevExpress.Xpo;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Base;

namespace ISOGDXAF.Dictonaries
{

    /// <summary>
    /// ������� ���������
    /// </summary>
    [Custom("Caption", "���������� ������ ���������"), System.ComponentModel.DefaultProperty("Name")]
    [ImageName("BO_Category"), NavigationItem("�������� �����������")]
    public class CoordSys : BaseObject
    {
        public CoordSys(Session session) : base(session) { }

        [Size(255), DisplayName("������������ ������� ���������")]
        public string Name { get; set; }
    }

    /// <summary>
    /// �������
    /// </summary>
    [Custom("Caption", "���������� ���������"), System.ComponentModel.DefaultProperty("Name")]
    [ImageName("BO_Category"), NavigationItem("�������� �����������")]
    public class Scale : BaseObject
    {
        public Scale(Session session) : base(session) { }

        [Size(255), DisplayName("�������")]
        public string Name { get; set; }
    }

    /// <summary>
    /// ��� �����
    /// </summary>
    [Custom("Caption", "��� �����"), System.ComponentModel.DefaultProperty("Name")]
    [ImageName("BO_Category"), NavigationItem("�������� �����������")]
    public class LawKind : BaseObject
    {
        public LawKind(Session session) : base(session) { }

        [Size(255), DisplayName("������������ ���� �����")]
        public string Name { get; set; }
    }
}