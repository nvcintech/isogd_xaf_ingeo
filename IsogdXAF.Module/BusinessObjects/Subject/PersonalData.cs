﻿using System;
using System.Collections.Generic;
using System.Drawing;

using DevExpress.Xpo;
using DevExpress.Xpo.Metadata;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;

namespace ISOGDXAF.Subject
{
    /// <summary>
    /// Личные данные
    /// </summary>
    [System.ComponentModel.DefaultProperty("FullName"), ImageName("BO_Person")]
    public class PersonalData : SubjectBase
    {

        private const string defaultFullNameFormat = "{LastName} {FirstName} {MiddleName}";
        private const string defaultBriefNameFormat = "{LastName} {FirstName}{MiddleName}";

        private string i_FirstName;
        private string i_LastName;
        private string i_MiddleName;

        private string i_FullName;


        private string i_BriefName;
        private string i_NameFrom;
        private string i_NameTo;
        private string i_NameBy;

        private static string fullNameFormat = defaultFullNameFormat;
        private static string ReplaceIgnoreCase(string str, string oldString, string newString)
        {
            string result = "";
            int lastNameEntryStartIndex = str.IndexOf(oldString, StringComparison.InvariantCultureIgnoreCase);
            if ((lastNameEntryStartIndex >= 0)
              && (str.IndexOf(newString) < 0))
            {
                if (lastNameEntryStartIndex > 0)
                {
                    result = str.Substring(0, lastNameEntryStartIndex);
                }
                result += newString;
                int oldStringLength = oldString.Length;
                if ((lastNameEntryStartIndex + oldStringLength) < str.Length)
                {
                    result += str.Substring(lastNameEntryStartIndex + oldStringLength, str.Length - lastNameEntryStartIndex - oldStringLength);
                }
            }
            else
            {
                result = str;
            }
            return result;
        }
        private static string ConvertIfItIsInOldFormat(string formatStr)
        {

            string result = ReplaceIgnoreCase(formatStr , "LastName", "{LastName}");
            result = ReplaceIgnoreCase(result, "FirstName", "{FirstName}");
            
            return ReplaceIgnoreCase(result, "MiddleName", "{MiddleName}");
        }
        public static string FullNameFormat
        {
            get { return fullNameFormat; }
            set
            {
                fullNameFormat = value;
                if (string.IsNullOrEmpty(fullNameFormat))
                {
                    fullNameFormat = defaultFullNameFormat;
                }
                else
                {
                    fullNameFormat = ConvertIfItIsInOldFormat(fullNameFormat);
                }
            }
        }
        public void SetFullName(string fullName)
        {
            FirstName = MiddleName = LastName = "";
            int index = fullName.IndexOf(',');
            if (index > 0)
            {
                fullName = fullName.Remove(0, index + 1).Trim() + " " + fullName.Substring(0, index);
            }
            string[] names = fullName.Split(' ');
            FirstName = names[0];
            if (names.Length == 2)
            {
                LastName = names[1];
            }
            else
                if (names.Length == 3)
                {
                    MiddleName = names[1];
                    LastName = names[2];
                }
                else
                {
                    for (int i = 2; i < names.Length; i++)
                    {
                        LastName += " " + names[i];
                    }
                }
        }

        //
        // !!! TODO: Добавить функцию автоматического формирования поля BriefName
        //
        // Написано мною
        public string GetBriefName()
        {
            string result = "";
            try
            {
                if (LastName.Length > 0)
                    result = defaultBriefNameFormat.Replace("{LastName}", LastName);
                try
                {
                    if (FirstName.Length > 0)
                        result = result.Replace("{FirstName}", FirstName[0].ToString().ToUpper() + ".");
                    try
                    {
                        if (MiddleName.Length > 0)
                            result = result.Replace("{MiddleName}", MiddleName[0].ToString().ToUpper() + ".");
                    }
                    catch
                    {
                        result = result.Replace("{MiddleName}", String.Empty);
                    }
                }
                catch
                {
                    result = result.Replace("{FirstName}", String.Empty);
                    result = result.Replace("{MiddleName}", String.Empty);
                }
            }
            catch { }
            return result;
        }
        [Size(64), DisplayName("Фамилия")]
        [ImmediatePostData]
        public string LastName
        {
            get { return i_LastName; }
            set
            {
                SetPropertyValue("LastName", ref i_LastName, value);
                OnChanged("LastName");
            }
        }

        [Size(64), DisplayName("Имя")]
        [ImmediatePostData]
        public string FirstName
        {
            get { return i_FirstName; }
            set
            {
                SetPropertyValue("FirstName", ref i_FirstName, value);
                OnChanged("FirstName");
            }
        }

      
        [Size(64), DisplayName("Отчество")]
        [ImmediatePostData]
        public string MiddleName
        {
            get { return i_MiddleName; }
            set
            {
                SetPropertyValue("MiddleName", ref i_MiddleName, value);
                OnChanged("MiddleName");
            }
        }

        [Size(255), DisplayName("ФИО")]
        public string FullName
        {
            get
            {
                i_FullName = ObjectFormatter.Format(fullNameFormat, this, EmptyEntriesMode.RemoveDelimeterWhenEntryIsEmpty);
                return i_FullName;
            }
            set
            {
                SetPropertyValue("FullName", ref i_FullName, value);
            }
        }

        [Size(64), DisplayName("Фамилия, инициалы")]
        [ImmediatePostData]
        public string BriefName
        {
            get
            {
                try
                {
                    i_BriefName = GetBriefName();
                }
                catch { }
                return i_BriefName;
            }
            set
            {
                SetPropertyValue("BriefName", ref i_BriefName, value);
                //// Инициируем падежи
                ////try
                ////{
                //    if (NameFrom.Length == 0)
                //        i_NameFrom = i_BriefName;
                //        //SetPropertyValue("NameFrom", ref i_BriefName, value);
                ////}
                ////catch { }
                //try
                //{
                //    if (NameTo.Length == 0)
                //        SetPropertyValue("NameTo", ref i_BriefName, value);
                //}
                //catch { }
                //try
                //{
                //    if (NameBy.Length == 0)
                //        SetPropertyValue("NameBy", ref i_BriefName, value);
                //}
                //catch { }
            }
        }

        [Size(255), DisplayName("ФИО в родительном падеже")]
        public string NameFrom
        {
            get {
                if (BriefName.Length > 0)
                    i_NameFrom = BriefName;
                return i_NameFrom; }
            set { SetPropertyValue("NameFrom", ref i_NameFrom, value); }
        }
        [Size(255), DisplayName("ФИО в дательном падеже")]
        public string NameTo
        {
            get
            {
                if (BriefName.Length > 0)
                    i_NameTo = BriefName; 
                return i_NameTo; }
            set { SetPropertyValue("NameTo", ref i_NameTo, value); }
        }
        [Size(255), DisplayName("ФИО в творительном падеже")]
        public string NameBy
        {
            get {
                if (BriefName.Length > 0)
                    i_NameBy = BriefName; 
                return i_NameBy;
            }
            set { SetPropertyValue("NameBy", ref i_NameBy, value); }
        }


        //[Size(255), DisplayName("Телефоны")]
        //public string ContactPhones { get; set; }

        //[DisplayName("E-mail")]
        //public string ContactMainEmail { get; set; }

        //[NonPersistent]
        
        public PersonalData(Session session) : base(session) { }
    }

}