﻿using System;
using System.Drawing;
using DevExpress.Xpo;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Persistent.Base.General;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base.Security;
//using DevExpress.ExpressApp.Security.Strategy;
//using ISOGDXAF.Address;
//using GenerateUserFriendlyId.Module.BusinessObjects;

namespace ISOGDXAF.Subject
{
    public partial class SubjectBase : BaseObject
    {
        private string _note;
        private string _contactPhones = String.Empty;
        private string _contactMainEmail = String.Empty;
        private string _contactAddress;
        private const string infoFormat = "т.:{ContactPhones}, адрес: {ContactAddress}";
        private const string adressFormat = "{ContactAddress}";
        private const string phonesFormat = "{ContactPhones}";



        [Size(255), DisplayName("Телефоны")]
        public string ContactPhones
        {
            get
            {
                string res = String.Empty;
                try
                {
                    foreach (Communication comm in Communications)
                        if (comm.CommunicationKind.Name == "Телефон" ||
                            comm.CommunicationKind.Name == "Сотовый телефон")
                        {
                            if (res != String.Empty)
                                res += ", ";
                            res += comm.Value;
                        }
                }
                catch { }
                return res;
            }
            set { SetPropertyValue("ContactPhones", ref _contactPhones, value); }
        }

        [Size(255), DisplayName("E-mail")]
        public string ContactMainEmail
        {
            get
            {
                string res = String.Empty;
                try
                {
                    foreach (Communication comm in Communications)
                        if (comm.CommunicationKind.Name == "E-mail")
                        {
                            if (res != String.Empty)
                                res += ", ";
                            res += comm.Value;
                        }
                }
                catch { }
                return res;
            }
            set { SetPropertyValue("ContactMainEmail", ref _contactMainEmail, value); }
        }
        [Size(1000), DisplayName("Адрес")]
        public string ContactAddress
        {
            get
            {
                //string res = String.Empty;
                //try
                //{
                //    //ISOGDXAF.Address.Address addr;
                //    //if (Addresses.Count > 0)
                //    //{
                //    //    addr = Addresses[0] as ISOGDXAF.Address.Address;
                //    //    res = addr.ShortAddress;
                //    //}
                //}
                //catch { }
                return _contactAddress;
            }
            set { SetPropertyValue("ContactAddress", ref _contactAddress, value); }
        }
        [Size(SizeAttribute.Unlimited), DisplayName("Примечание")]
        public string Note
        {
            get { return _note; }
            set { SetPropertyValue("Note", ref _note, value); }
        }
        [Size(1000), System.ComponentModel.Browsable(false)]
        public string Info
        {
            get
            {
                return ObjectFormatter.Format(infoFormat, this, EmptyEntriesMode.RemoveDelimeterWhenEntryIsEmpty);
            }
        }
        [Size(1000), System.ComponentModel.Browsable(false)]
        public string address
        {
            get
            {
                return ObjectFormatter.Format(adressFormat, this, EmptyEntriesMode.RemoveDelimeterWhenEntryIsEmpty);
            }
        }
        [Size(510), System.ComponentModel.Browsable(false)]
        public string phones
        {
            get
            {
                return ObjectFormatter.Format(phonesFormat, this, EmptyEntriesMode.RemoveDelimeterWhenEntryIsEmpty);
            }
        }
        //[Association("SubjectBase-Address", typeof(ISOGDXAF.Address.Address))]
        //[DisplayName("Адреса")]
        //public XPCollection Addresses
        //{
        //    get { return GetCollection("Addresses"); }
        //}

        [Association("SubjectBase-Communication", typeof(Communication))]
        [DisplayName("Средства связи")]
        public XPCollection Communications
        {
            get { return GetCollection("Communications"); }
        }


        public SubjectBase(Session session) : base(session) { }
    }
}
