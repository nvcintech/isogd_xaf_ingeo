﻿using System;
using DevExpress.Xpo;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Base;

namespace ISOGDXAF.Subject
{

    /// <summary>
    /// Вид средства связи
    /// </summary>
    [Custom("Caption", "Вид средства связи"), System.ComponentModel.DefaultProperty("Name")]
    [NavigationItem("Основные справочники")]
    public class CommunicationKind : BaseObject
    {
        private string _name;

        [Size(255), DisplayName("Наименование")]
        public string Name
        {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        public override string ToString()
        {
            return Name;
        }

        public CommunicationKind(Session session) : base(session) { }
    }


}