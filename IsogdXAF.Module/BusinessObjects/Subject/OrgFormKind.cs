using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Xpo;

namespace ISOGDXAF.Subject
{
    /// <summary>
    /// ��������������-�������� �����
    /// </summary>
    [Custom("Caption", "��������������-�������� �����")]
    [System.ComponentModel.DefaultProperty("BriefName")]
    [ImageName("BO_Category"), NavigationItem("�������� �����������")]
    public class OrgFormKind : BaseObject {
        private string _name;
        private string _briefName;

        [Size(32), DisplayName("������� ������������")]
        [ImmediatePostData]
        public string BriefName {
            get { return _briefName; }
            set { SetPropertyValue("BriefName", ref _briefName, value); }
        }
        [Size(255), DisplayName("������������")]
        [ImmediatePostData]
        public string Name {
            get { return _name; }
            set { SetPropertyValue("Name", ref _name, value); }
        }

        public OrgFormKind(Session session) : base(session) { }
    }
}
