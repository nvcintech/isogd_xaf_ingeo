using System;
using DevExpress.Xpo;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.Persistent.BaseImpl;
using ISOGDXAF.Enums;
using DevExpress.Persistent.Base.General;
using ISOGDXAF.OrgStructure;

namespace ISOGDXAF.OrgStructure
{
    /// <summary>
    /// ������������� �����������
    /// </summary>
    [NavigationItem("��������� �����������")]
    public class Municipality : BaseObject, IHCategory
    {
        public Municipality(Session session) : base(session) { }

        [DisplayName("��� �������������� �����������")]
        public MunicipalityKind MunicipalityKind { get; set; }

        [Size(255), DisplayName("������������")]
        public string Name { get; set; }

        [Size(255), DisplayName("��� �����")]
        public string OKTMO { get; set; }

        [Size(255), DisplayName("����������� �����")]
        public string CadastreNo { get; set; }

        [Size(255), DisplayName("���������� ��������")]
        public string ActivityArea { get; set; }
        
        [Size(SizeAttribute.Unlimited), DisplayName("����������")]
        public string Notes { get; set; }

        [Association("MunicipalityParent-MunicipalityChild")]
        [DisplayName("���������������-��������������� �������")]
        public XPCollection<Municipality> Children
        {
            get { return GetCollection<Municipality>("Children"); }
        }
        

        [Association, DisplayName("����� ���������� ���������������-��������������� ��������")]
        public XPCollection<MainDepartment> MainDepartment
        {
            get { return GetCollection<MainDepartment>("MainDepartment"); }
        }

        System.ComponentModel.IBindingList ITreeNode.Children
        {
            get { return Children as System.ComponentModel.IBindingList; }
        }

        private Municipality parent;

        [Persistent, Association("MunicipalityParent-MunicipalityChild")]
        [DisplayName("������������ ������������� �����������")]
        public Municipality Parent
        {
            get { return parent; }
            set
            {
                try
                {
                    parent = value;
                    OnChanged("Parent");
                }
                catch { }
            }
        }
        ITreeNode IHCategory.Parent
        {
            get { return Parent as IHCategory; }
            set { Parent = value as Municipality; }
        }
        ITreeNode ITreeNode.Parent
        {
            get { return Parent as ITreeNode; }
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
        }
    }
}