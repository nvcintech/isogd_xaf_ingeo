using System;
using DevExpress.Xpo;
using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Persistent.Base.General;
using System.Collections.Generic;
using ISOGDXAF.Isogd;
//using GenerateUserFriendlyId.Module.BusinessObjects;

namespace ISOGDXAF.OrgStructure
{


    [Custom("Caption", "����� ���������� ���������������-��������������� ��������"), NavigationItem("��������� �����������")]
    [System.ComponentModel.DefaultProperty("Name"), ImageName("BO_Department")]
    public class MainDepartment : BaseObject
    {
        public MainDepartment(Session session) : base(session) { }

        private string i_Name;
        
        [Size(255), DisplayName("������������")]
        public string Name
        {
            get { return i_Name; }
            set { SetPropertyValue("Name", ref i_Name, value); }
        }
        [Association, DisplayName("���������������-��������������� �������")]
        public Municipality Municipality { get; set; }

        [Association]
        [DisplayName("����������� �������������")]
        public XPCollection<Departament> Departament
        {
            get { return GetCollection<Departament>("Departament"); }
        }

        [Association]
        [DisplayName("���������� ��������� ��� ������ �������� �����")]
        public XPCollection<MunicipalityBankDetails> MunicipalityBankDetails
        {
            get { return GetCollection<MunicipalityBankDetails>("MunicipalityBankDetails"); }
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        protected override void OnSaved()
        {
            base.OnSaved();
        }

        protected override void OnDeleting()
        {
            base.OnDeleting();
        }
    }
}
