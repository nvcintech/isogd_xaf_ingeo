﻿using System;

using DevExpress.Xpo;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Persistent.Base.General;
using System.Collections.Generic;
//using GenerateUserFriendlyId.Module.BusinessObjects;

namespace ISOGDXAF.OrgStructure
{

    /// <summary>
    /// Подразделение
    /// </summary>
    [Custom("Caption", "Подразделения"), NavigationItem("Структура организации")]
    [System.ComponentModel.DefaultProperty("BriefName"), ImageName("BO_Department")]
    public class Departament : BaseObject, IHCategory
    {

        private const int departamentSeqNameLength = 2;

        private string i_Code;
        private string i_Name;
        private string i_BriefName;
        private string i_Note;
        private Employee i_Manager;
        private Employee i_SubManager;


        [Size(32), DisplayName("Код")]
        public string Code
        {
            get { return i_Code; }
            set { SetPropertyValue("Code", ref i_Code, value); }
        }

        [Size(255), DisplayName("Полное наименование")]
        public string Name
        {
            get { return i_Name; }
            set { SetPropertyValue("Name", ref i_Name, value); }
        }
        [Size(255), DisplayName("Сокращенное наименование")]
        public string BriefName
        {
            get { return i_BriefName; }
            set { SetPropertyValue("BriefName", ref i_BriefName, value); }
        }
        [DisplayName("Руководитель отдела")]
        public Employee Manager
        {
            get { return i_Manager; }
            set { try { SetPropertyValue("Manager", ref i_Manager, value); } catch { } }
        }
        [DisplayName("Заместитель руководителя отдела")]
        public Employee SubManager
        {
            get { return i_SubManager; }
            set { try { SetPropertyValue("SubManager", ref i_SubManager, value); } catch { } }
        }
        [Association, DisplayName("Орган управления административно-территориальной единицей")]
        public MainDepartment MainDepartment { get; set; }

        [Size(4000), DisplayName("Примечание")]
        public string Note
        {
            get { return i_Note; }
            set { SetPropertyValue("Note", ref i_Note, value); }
        }

        [Association("Departament-Employee", typeof(Employee))]
        [DisplayName("Сотрудники отдела")]
        public XPCollection<Employee> Employers
        {
            get { return GetCollection<Employee>("Employers"); }
        }
        [Association("DepartamentParent-DepartamentChild")]
        [DisplayName("Подчиненные подразделения")]
        public XPCollection<Departament> Children
        {
            get { return GetCollection<Departament>("Children"); }
        }


        System.ComponentModel.IBindingList ITreeNode.Children
        {
            get { return Children as System.ComponentModel.IBindingList; }
        }

        private Departament parent;

        [Persistent, Association("DepartamentParent-DepartamentChild")]
        [DisplayName("Вышестоящее подразделение")]
        public Departament Parent
        {
            get { return parent; }
            set
            {
                try
                {
                    parent = value;
                    OnChanged("Parent");
                }
                catch { }
            }
        }
        ITreeNode IHCategory.Parent
        {
            get { return Parent as IHCategory; }
            set { Parent = value as Departament; }
        }
        ITreeNode ITreeNode.Parent
        {
            get { return Parent as ITreeNode; }
        }

        public override string ToString()
        {
            return Name;
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
        }

        protected override void OnSaved()
        {
            base.OnSaved();
        }

        protected override void OnDeleting()
        {
            base.OnDeleting();
        }


        public Departament(Session session) : base(session) { }
    }

   
}
