using System;
using DevExpress.Xpo;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;

namespace ISOGDXAF.OrgStructure
{
    /// <summary>
    /// ��� ���������������-��������������� �������
    /// </summary>
    //[NavigationItem("�������������� �����")]
    [System.ComponentModel.DefaultProperty("Name")]
    public class MunicipalityKind : BaseObject
    {
        public MunicipalityKind(Session session) : base(session) { }

        [Size(8), DisplayName("���")]
        public string Code { get; set; }

        [Size(255), DisplayName("������������")]
        public string Name { get; set; }
    }
}