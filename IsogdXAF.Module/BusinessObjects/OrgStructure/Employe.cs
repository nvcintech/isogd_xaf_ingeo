﻿using System;
using System.Drawing;
using DevExpress.Xpo;

using DevExpress.ExpressApp;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Validation;
using DevExpress.Persistent.Base.General;
using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base.Security;
using DevExpress.ExpressApp.Security.Strategy;
using ISOGDXAF.Subject;
//using GenerateUserFriendlyId.Module.BusinessObjects;

namespace ISOGDXAF.OrgStructure
{
    /// <summary>
    /// Сотрудник
    /// </summary>
    [Custom("Caption", "Сотрудник"), NavigationItem("Структура организации")]
    [System.ComponentModel.DefaultProperty("BriefName"), ImageName("BO_Employee")]
    public class Employee : PersonalData
    {

        private string i_Code;
        private string i_UserName;
        private string i_Position;
        private Departament i_Departament;
        private SecuritySystemUser i_user;


        [Size(32), DisplayName("Код сотрудника")]
        public string Code
        {
            get { return i_Code; }
            set { SetPropertyValue("Code", ref i_Code, value); }
        }
        [Size(255), DisplayName("Псевдоним")]
        public string UserName
        {
            get { return i_UserName; }
            set { SetPropertyValue("UserName", ref i_UserName, value); }
        }
        [Size(255), DisplayName("Должность")]
        public string Position
        {
            get { return i_Position; }
            set { try { SetPropertyValue("Position", ref i_Position, value); } catch { } }
        }


        [DisplayName("Пользователь системы")]
        public SecuritySystemUser SysUser
        {
            get { return i_user; }
            set { SetPropertyValue("SysUser", ref i_user, value); }
        }
        //[DisplayName("Должность")]
        //[DataSourceProperty("Departament.Positions")]
        //public Position Position
        //{
        //    get { return i_Position; }
        //    set { try { SetPropertyValue("Position", ref i_Position, value); } catch { } }
        //}
        [DisplayName("Отдел")]
        [Association("Departament-Employee", typeof(Departament))]
        public Departament Departament
        {
            get { return i_Departament; }
            set { try { SetPropertyValue("Departament", ref i_Departament, value); } catch { } }
        }


        [NonPersistent, System.ComponentModel.Browsable(false)]
        public object Id
        {
            get { return Oid; }
        }
        [NonPersistent, System.ComponentModel.Browsable(false)]
        public string Caption
        {
            get { return BriefName; }
            set { BriefName = value; }
        }

        [NonPersistent, System.ComponentModel.Browsable(false)]
        public Guid UserId
        {
            get
            {
                User usr = Session.FindObject<User>(new BinaryOperator("Employee", Id));
                if (usr != null)
                    return usr.Oid;
                else return Guid.Empty;
            }
        }


        public Employee GetCurrentEmployee()
        {
            if (SecuritySystem.CurrentUser != null)
            {
                Employee currentEmpl = Session.FindObject<Employee>(new BinaryOperator("SysUser", SecuritySystem.CurrentUserId));
                if (currentEmpl != null)
                    return currentEmpl;
                else
                    return null;
            }
            else return null;
        }
        //private string GetEmployeeCode() {
        //    //try { seqNo = XpoSequencer.GetNextValue(Session.DataLayer, employeeSeqName); }
        //    //catch { }
        //    //if (SequentialNumber == 0)
        //    //    SequentialNumber++;
        //    string codeNo = SequentialNumber.ToString();
        //    //while (codeNo.Length < employeeSeqNameLength)
        //    //    codeNo = "0" + codeNo;
        //    return codeNo;
        //}

        //private void SetEmployeeCode() {
        //    try {
        //        string codeNo = this.GetEmployeeCode();
        //        this.i_Code = codeNo;
        //        SetPropertyValue("Code", ref i_Code, codeNo);
        //    }
        //    catch { }
        //}

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Дата регистрации
            // Номер сотрудника
            //string codeNo = GetLastSequenceNo().ToString();
            //while (codeNo.Length < employeeSeqNameLength)
            //    codeNo = "0" + codeNo;
            //Code = codeNo;
        }

        protected override void OnSaved()
        {
            base.OnSaved();
        }

        public Employee(Session session) : base(session) { }
    }
}
