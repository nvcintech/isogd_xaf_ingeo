﻿using System;
using DevExpress.Xpo;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using DevExpress.ExpressApp;
using DevExpress.Data.Filtering;
using DevExpress.ExpressApp.Editors;

namespace ISOGDXAF.Enums
{

    /// <summary>
    /// Тип субъекта
    /// </summary>
    public enum RequesterType
    {
        физ = 0,
        юр = 1
    }



    /// <summary>
    /// Тип раздела ИСОГД
    /// </summary>
    public enum PartitionType
    {
        Основной = 0,
        Дополнительный = 1
    }

    /// <summary>
    /// Статус документа
    /// </summary>
    public enum DocumentStatus
    {
        Действует = 0,
        Изменен = 1,
        Отменен = 2
    }

    /// <summary>
    /// Гриф секретности
    /// </summary>
    public enum SecretLevel
    {
        НеСекретно = 0,
        ДляСлужебногоПользования = 1,
        Секретно = 2,
        СовершенноСекретно = 3
    }

    /// <summary>
    /// Тип раздела ИСОГД
    /// </summary>
    public enum ReasonKind
    {
        СопроводительноеПисьмо = 0,
        ДокументОснование = 1
    }

    /// <summary>
    /// Тип книги ИСОГД
    /// </summary>
    public enum IsogdBooksKind
    {
        КнигаРегистрации = 0,
        КнигаУчетаСведений = 1,
        КнигаХранения = 2,
        КнигаУчетаЗаявок = 3,
        КнигаПредоставленияСведений = 4
    }

    /// <summary>
    /// Класс верхнего уровня
    /// </summary>
    public enum IsogdDocumentMainClass
    {
        ДокументыТерриториальногоПланирования = 0,
        ПравилаЗемлепользованияИЗастройкиВнесениеВНихИзменений = 1,
        ДокументацияПоПланировкеТерриторий = 2,
        МатериалыОПриродныхИТехногенныхУсловиях = 3,
        ДокументыОбИзъятииЗемельногоУчасткаДляГосударственныхИлиМуниципальныхНужд = 4,
        ДокументыВДелеОЗастроенныхИПодлежащихЗастройкеЗемельныхУчастках = 5,
        Иное = 6
    }

    /// <summary>
    /// Статус услуги
    /// </summary>
    public enum ServiceStatus
    {
        ЗаПлату = 0,
        Бесплатно = 1
    }
}