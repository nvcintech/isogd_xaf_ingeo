using System;
using DevExpress.Xpo;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.Persistent.BaseImpl;
using ISOGDXAF.Enums;

namespace ISOGDXAF.Isogd
{
    [NavigationItem("������� � ��������� �����"), System.ComponentModel.DefaultProperty("descript")]
    public class IsogdPartition : BaseObject
    {

        [Size(255), System.ComponentModel.Browsable(false)]
        private string descript
        {
            get
            {
                return String.Format("������ ����� {0} {1}", PartNo, Name);
            }
        }

        public IsogdPartition(Session session) : base(session) { }
        [Size(255), DisplayName("����� �������")]
        public string PartNo { get; set; }

        [Size(255), DisplayName("������������ �������")]
        public string Name { get; set; }

        [Size(255), DisplayName("������ ������������ �������")]
        public string FullName { get; set; }

        [DisplayName("��� �������")]
        public PartitionType PartitionType { get; set; }
        


        [Size(SizeAttribute.Unlimited), DisplayName("����������")]
        public string Notes { get; set; }

        
        [Association, DisplayName("��������� �����")]
        public XPCollection<IsogdDocument> IsogdDocument
        {
            get { return GetCollection<IsogdDocument>("IsogdDocument"); }
        }

        [Association, DisplayName("����� ����� ���������� �����")]
        public XPCollection<IsogdMap> IsogdMap
        {
            get { return GetCollection<IsogdMap>("IsogdMap"); }
        }

        [Association, DisplayName("����� �������� �����")]
        public XPCollection<IsogdStorageBook> IsogdStorageBook
        {
            get { return GetCollection<IsogdStorageBook>("IsogdStorageBook"); }
        }

        [Association, DisplayName("�������� ����������� ���������� �������")]
        public XPCollection<IsogdRegistrationCard> IsogdRegistrationCard
        {
            get { return GetCollection<IsogdRegistrationCard>("IsogdRegistrationCard"); }
        }
    }
}