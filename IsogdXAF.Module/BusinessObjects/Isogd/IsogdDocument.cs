using System;
using DevExpress.Xpo;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.Persistent.BaseImpl;
using ISOGDXAF.General;
using ISOGDXAF.OrgStructure;
using DevExpress.ExpressApp;
using DevExpress.Data.Filtering;
using ISOGDXAF.Enums;
using ISOGDXAF.Subject;
using ISOGDXAF.Land;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.Editors;

namespace ISOGDXAF.Isogd
{
    [NavigationItem("������� � ��������� �����")]
    [System.ComponentModel.DefaultProperty("FullName")]
    public class IsogdDocument : AttachBase
    {
        public IsogdDocument(Session session) : base(session) { }

        private string i_docName;
        private string i_regNo;
        private IsogdDocumentNameKind i_docNameKind;
        private DateTime i_placementDate;
        private Employee i_Employee;
        private DocumentStatus i_DocumentStatus;
        private SecretLevel i_SecretLevel;

        [System.ComponentModel.Browsable(false)]
        public string FullName
        {
            get
            {
                return String.Format("���.� {0}, ���� ���������� {1},"
            + " ������������: {2}", RegNo, PlacementDate.ToShortDateString(), DocName);
            }
        }
        private Municipality i_Municipality;
        [DisplayName("������������� �����������")]
        [ImmediatePostData]
        public Municipality Municipality
        {
            get {
                try { i_Municipality = GetMunicipality(); }
                catch { }
                return i_Municipality; }
            set { SetPropertyValue("Municipality", ref i_Municipality, value); }
        }

        /// <summary>
        /// �������� ������������� ����������� �� ����������
        /// </summary>
        /// <returns></returns>
        public Municipality GetMunicipality()
        {
            Municipality mun = null;
            if (Empl != null)
                if (Empl.Departament != null)
                    if (Empl.Departament.MainDepartment != null)
                        if (Empl.Departament.MainDepartment.Municipality != null)
                            mun = Empl.Departament.MainDepartment.Municipality;
            return mun;
        }

        private IsogdDocumentClass i_IsogdDocumentClass;
        [DisplayName("����� ��������� �����")]
        [ImmediatePostData]
        public IsogdDocumentClass IsogdDocumentClass
        {
            get { return i_IsogdDocumentClass; }
            set { SetPropertyValue("IsogdDocumentClass", ref i_IsogdDocumentClass, value); }
        }

        private IsogdPartition i_IsogdPartition;
        [Association, DisplayName("������ �����")]
        [ImmediatePostData]
        public IsogdPartition IsogdPartition
        {
            get {
                if (IsogdDocumentClass != null)
                    if (IsogdDocumentClass.IsogdPartition != null)
                        i_IsogdPartition = IsogdDocumentClass.IsogdPartition;
                return i_IsogdPartition; }
            set { SetPropertyValue("IsogdPartition", ref i_IsogdPartition, value); }
        }

        [Size(255), DisplayName("��������������� ����� ���������")]
        public string RegNo
        {
            get {
                return i_regNo; }
            set { SetPropertyValue("RegNo", ref i_regNo, value); }
            
        }
        public string GetRegNo()
        {
            string res = "";
            if (Municipality != null)
                if (Municipality.OKTMO != null)
                    res = Municipality.OKTMO;
            if (IsogdPartition != null)
            {
                if (IsogdPartition.PartNo != null)
                    res += IsogdPartition.PartNo;
                string count = Convert.ToString(IsogdPartition.IsogdDocument.Count + 1);
                if (count.Length == 1)
                    count = String.Concat("000", count);
                if (count.Length == 2)
                    count = String.Concat("00", count);
                if (count.Length == 3)
                    count = String.Concat("0", count);
                if (count.Length > 3 )
                    count = count;
                if (count.Length == 0)
                    count = String.Concat("0000", count);
                res += count;
            }
            return res;
        }
        private string i_CadNo;
        [Size(510),DisplayName("����������� ������ ��������")]
        [ImmediatePostData]
        [Appearance("isPart8", Visibility = ViewItemVisibility.Hide,
            Criteria = "IsogdPartition.Name != '����������� � ���������� ��������� ��������� �������'", Context = "DetailView")]
        public string CadNo
        {
            get { return i_CadNo; }
            set { SetPropertyValue("CadNo", ref i_CadNo, value); }
        }
        [DisplayName("������������ ��������� (���������)")]
        [ImmediatePostData]
        public IsogdDocumentNameKind IsogdDocumentNameKind
        {
            get { return i_docNameKind; }
            set
            {
                try { SetPropertyValue("IsogdDocumentNameKind", ref i_docNameKind, value); }
                catch { }
                OnChanged("IsogdDocumentNameKind");
            }
        }
        
        [Size(255), DisplayName("������������ ���������")]
        public string DocName
        {
            get
            {
                string na = "", no = "", da = "";
                try
                {
                    if (i_docNameKind != null)
                        if (i_docNameKind.Name.Length > 0)
                            na = i_docNameKind.Name;
                    if (DocNo != null)
                        if (DocNo.Length > 0)
                            no = " � " + DocNo;
                    if (StatementDate != DateTime.MinValue)
                        da = " �� " + StatementDate.ToShortDateString();
                    i_docName = na + no + da;
                }
                catch { }

                return i_docName;
            }
            set
            {
                SetPropertyValue("DocName", ref i_docName, value);
            }
        }
        [DisplayName("���� ���������� ���������")]
        public DateTime PlacementDate
        {
            get
            {
                try {
                    if (i_placementDate == DateTime.MinValue)
                        i_placementDate = DateTime.Now.Date;
                }
                catch { }
                return i_placementDate;
            }
            set
            {
                SetPropertyValue("PlacementDate", ref i_placementDate, value);
            }
        }
        [DisplayName("���������, ������������ ��������")]
        [ImmediatePostData]
        public Employee Empl
        {
            get { return i_Employee; }
            set { SetPropertyValue("Empl", ref i_Employee, value); }
        }
        
        [DisplayName("����� ���������"), Size(255)]
        public string DocNo { get; set; }

        [DisplayName("���� ����������� ���������")]
        public DateTime StatementDate { get; set; }

        [DisplayName("������ ���������")]
        public DocumentStatus DocumentStatus
        {
            get { return i_DocumentStatus; }
            set { SetPropertyValue("DocumentStatus", ref i_DocumentStatus, value); }
        }

        [DisplayName("���� �����������")]
        public SecretLevel SecretLevel
        {
            get { return i_SecretLevel; }
            set { SetPropertyValue("SecretLevel", ref i_SecretLevel, value); }
        }

        [DisplayName("���������� ��������"), Size(255)]
        public string ActivityArea { get; set; }

        [DisplayName("�����������, ������������� ��������")]
        public Org Developer { get; set; }

        [DisplayName("�����������, ����������� ��������")]
        public Org StatementOrg { get; set; }

        [Size(SizeAttribute.Unlimited), DisplayName("����������")]
        public string Notes { get; set; }

        [Association, DisplayName("����� ����� ���������� �����")]
        public XPCollection<IsogdMap> IsogdMap
        {
            get { return GetCollection<IsogdMap>("IsogdMap"); }
        }

        [Association, DisplayName("��������� �������, ��������� � ����������")]
        public XPCollection<Lot> Lot
        {
            get { return GetCollection<Lot>("Lot"); }
        }

        [Association("IsogdDocument-IsogdStorageBook"), DisplayName("����� �������� �����")]
        public XPCollection<IsogdStorageBook> IsogdStorageBook
        {
            get { return GetCollection<IsogdStorageBook>("IsogdStorageBook"); }
        }

        [Association, DisplayName("���� ���� ��������")]
        public XPCollection<IsogdStorageBookTome> IsogdStorageBookTome
        {
            get { return GetCollection<IsogdStorageBookTome>("IsogdStorageBookTome"); }
        }

        [Association, DisplayName("�������������� �������� �����")]
        [System.ComponentModel.Browsable(false)]
        public XPCollection<IsogdRequestCard> IsogdRequestCard
        {
            get { return GetCollection<IsogdRequestCard>("IsogdRequestCard"); }
        }


        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
            if (SecuritySystem.CurrentUser != null)
            {
                Employee currentEmpl = Session.FindObject<Employee>(new BinaryOperator("SysUser", SecuritySystem.CurrentUserId));
                if (currentEmpl != null)
                    i_Employee = currentEmpl;
            }
            //try
            //{
            //    i_regNo = GetRegNo();
            //}
            //catch { }
            //if (i_requesterType)
        }
        protected override void OnSaving()
        {
            //base.OnSaving();
            //IsogdDocument isogdDoc;
            //string docname = "";
            //if (DocNo == null || DocNo == "")
            //    throw new Exception("�� ������ ����� ���������!");
            //if (IsogdDocumentClass == null)
            //    throw new Exception("�� ������ ����� ���������!");
            //if (IsogdDocumentClass.Code == null || IsogdDocumentClass.Code == string.Empty)
            //    throw new Exception("� ������ �� ������ ���!");
            //if (StatementDate != DateTime.MinValue)
            //{
            //    isogdDoc = Session.FindObject<IsogdDocument>(CriteriaOperator.Parse(string.Format("DocNo = '{0}'and IsogdDocumentClass.Code = '{1}'and StatementDate = '{2}'",
            //           DocNo, IsogdDocumentClass.Code, StatementDate)));
            //    docname = String.Format("� {0} �� {1}, �����: {2}", DocNo, StatementDate.ToShortDateString(), IsogdDocumentClass.Code + ":" + IsogdDocumentClass.Name);
            //}
            //else
            //{
            //    isogdDoc = Session.FindObject<IsogdDocument>(CriteriaOperator.Parse("DocNo = {0} and IsogdDocumentClass.Code = {1}",
            //        DocNo, IsogdDocumentClass.Code));
            //    docname = String.Format("� {0} �� '��� ����', �����: {1}", DocNo, IsogdDocumentClass.Code + ":" + IsogdDocumentClass.Name);
            //}
            //if (isogdDoc != null)
            //    if (isogdDoc != this)
            //        throw new Exception(string.Format("�������� ����� '{0}' ��� ���������� � �������. ��������� ������������ ����������.", docname));
            //try
            //{
            //    if (this.RegNo == String.Empty || this.RegNo == null)
            //        this.RegNo = GetRegNo();
            //    //this.Save();
            //}
            //catch { }
        }
        // protected override void OnSaved()
        //{
        //     base.OnSaved();
        //    try
        //    {
        //        if (this.RegNo == String.Empty || this.RegNo == null)
        //            this.RegNo = GetRegNo();
        //        this.Save();
        //    }
        //    catch { }
        //}
    }
}