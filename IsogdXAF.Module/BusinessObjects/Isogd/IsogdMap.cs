using System;
using DevExpress.Xpo;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.Persistent.BaseImpl;
using ISOGDXAF.Enums;
using ISOGDXAF.Dictonaries;
using ISOGDXAF.General;

namespace ISOGDXAF.Isogd
{


    [NavigationItem("������� � ��������� �����")]
    public class IsogdMap : AttachBase
    {
        public IsogdMap(Session session) : base(session) { }


        [Association, DisplayName("������ �����")]
        public IsogdPartition IsogdPartition { get; set; }

        [Association, DisplayName("�������� �����")]
        public IsogdDocument IsogdDocument { get; set; }

        [Size(255), DisplayName("����������������� �����")]
        public string IdNo { get; set; }

        [DisplayName("����� ����� � ���������")]
        public int inDocNo { get; set; }

        [Size(255), DisplayName("������������")]
        public string Name { get; set; }

        [DisplayName("���� ���������� �����/�����")]
        public DateTime DistributionDate { get; set; }

        [DisplayName("��� �����/�����")]
        public IsogdMapKind IsogdMapKind { get; set; }

        [DisplayName("���� ������������ �����/�����")]
        public DateTime StatementDate { get; set; }

        [DisplayName("�������")]
        public Scale GeneralGauge { get; set; }

        [Size(255), DisplayName("���������� ��������")]
        public string MapArea { get; set; }

        [DisplayName("������� ���������")]
        public CoordSys GeneralCoordSys { get; set; }

        [Size(SizeAttribute.Unlimited), DisplayName("����������")]
        public string Notes { get; set; }
        
        
    }
}