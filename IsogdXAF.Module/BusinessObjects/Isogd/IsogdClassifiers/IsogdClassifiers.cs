using System;
using DevExpress.Xpo;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;
using ISOGDXAF.Enums;

namespace ISOGDXAF.Isogd
{

    /// <summary>
    /// ����� ��������� �����
    /// </summary>
    [NavigationItem("�������������� �����"), System.ComponentModel.DefaultProperty("descript")]
    public class IsogdDocumentClass : BaseObject
    {

        public IsogdDocumentClass(Session session) : base(session) { }

        [Size(255), System.ComponentModel.Browsable(false)]
        public string descript
        {
            get
            {
                return String.Format("����� ��������� ����� {0}: {1}", Code, Name);
            }
        }

        [Size(255), DisplayName("���")]
        public string Code { get; set; }

        [Size(255), DisplayName("������������ ������")]
        public string Name { get; set; }

        [Size(255), DisplayName("����� �������� ������")]
        public IsogdDocumentMainClass IsogdDocumentMainClass { get; set; }

        [Size(SizeAttribute.Unlimited), DisplayName("����������")]
        public string Notes { get; set; }

        [DisplayName("������ �����")]
        public IsogdPartition IsogdPartition { get; set; }

        [DisplayName("����� ��������")]
        public IsogdStorageBook IsogdStorageBook { get; set; }

    }

    /// <summary>
    /// ����� �������� ������
    /// </summary>
    //public class IsogdDocumentMainClass : BaseObject
    //{
    //    public IsogdDocumentMainClass(Session session) : base(session) { }
    //    [Size(255), DisplayName("���")]
    //    public string Code { get; set; }

    //    [Size(255), DisplayName("������������ ������")]
    //    public string Name { get; set; }
    //}

}