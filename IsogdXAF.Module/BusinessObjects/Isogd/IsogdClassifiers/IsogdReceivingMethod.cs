using System;
using DevExpress.Xpo;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;

namespace ISOGDXAF.Isogd
{
    /// <summary>
    /// ������ �������� ���������
    /// </summary>
    [NavigationItem("�������������� �����")]
    public class IsogdReceivingMethod : BaseObject
    {
        public IsogdReceivingMethod(Session session) : base(session) { }

        [Size(8), DisplayName("���")]
        public string Code { get; set; }

        [Size(255), DisplayName("������������")]
        public string Name { get; set; }
    }
}