using System;
using DevExpress.Xpo;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;

namespace ISOGDXAF.Isogd
{
    /// <summary>
    /// ��� ������������ ��������� �����
    /// </summary>
    //[NavigationItem("�������������� �����")]
    public class IsogdDocumentNameKind : BaseObject
    {
        public IsogdDocumentNameKind(Session session) : base(session) { }

        [Size(255), DisplayName("������������ ��������� (���������)")]
        public string Name { get; set; }
    }
}