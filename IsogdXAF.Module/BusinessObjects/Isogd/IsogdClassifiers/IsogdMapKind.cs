using System;
using DevExpress.Xpo;
using DevExpress.Persistent.Base;
using DevExpress.Persistent.BaseImpl;

namespace ISOGDXAF.Isogd
{
    /// <summary>
    /// ��� �����/����� �����
    /// </summary>
    [NavigationItem("�������������� �����")]
    public class IsogdMapKind : BaseObject
    {
        public IsogdMapKind(Session session) : base(session) { }

        [Size(255), DisplayName("���")]
        public string Code { get; set; }

        [Size(255), DisplayName("������������")]
        public string Name { get; set; }

        [Size(SizeAttribute.Unlimited), DisplayName("����������")]
        public string Notes { get; set; }
    }

}