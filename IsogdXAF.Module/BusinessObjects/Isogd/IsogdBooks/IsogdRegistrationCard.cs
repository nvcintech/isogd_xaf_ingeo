using System;
using DevExpress.Xpo;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.Persistent.BaseImpl;
using ISOGDXAF.General;
using ISOGDXAF.Enums;
using ISOGDXAF.OrgStructure;
using DevExpress.ExpressApp;
using DevExpress.Data.Filtering;

namespace ISOGDXAF.Isogd
{

    /// <summary>
    /// �������� ����������� ��������� �����
    /// </summary>
    [NavigationItem("����� ����� �����"), System.ComponentModel.DefaultProperty("descript")]
    public class IsogdRegistrationCard : BaseObject
    {
        public IsogdRegistrationCard(Session session) : base(session) { }

        private DateTime i_RecordDate;
        private Employee i_Employee;

        [Size(255), System.ComponentModel.Browsable(false)]
        public string descript
        {
            get
            {
                return String.Format("�������� ����������� ��������� ����� � {0} �� {1}", RecNo, RecordDate.ToShortDateString());
            }
        }

        [Association, DisplayName("������ �����")]
        public IsogdPartition IsogdPartition { get; set; }

        private string i_tomeNo;
        [DisplayName("����� ���� ����� �����������")]
        public string TomeNo
        {
            get { return i_tomeNo; }
            set { SetPropertyValue("TomeNo", ref i_tomeNo, value); }
        }

        [Persistent("RecNo")]
        private string RecNoCore;
        [PersistentAlias("RecNoCore")]
        [DisplayName("����� ��������")]
        public string RecNo
        {
            get { return RecNoCore; }
        }

        [DisplayName("���� ������")]
        public DateTime RecordDate
        {
            get { return i_RecordDate; }
            set { SetPropertyValue("RecordDate", ref i_RecordDate, value); }
        }

        [DisplayName("���������, ������������� ����")]
        public Employee Empl
        {
            get { return i_Employee; }
            set { SetPropertyValue("Empl", ref i_Employee, value); }
        }
        [DisplayName("�������� �����")]
        public IsogdDocument IsogdDocument { get; set; }

        [DisplayName("���������������� ������")]
        public IsogdLetter IsogdLetter { get; set; }

        [Size(SizeAttribute.Unlimited), DisplayName("����������")]
        public string Notes { get; set; }

        [Association, DisplayName("������ � ����� ����� ��������")]
        public XPCollection<IsogdInDataCard> IsogdInDataCard
        {
            get { return GetCollection<IsogdInDataCard>("IsogdInDataCard"); }
        }

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
            if (i_RecordDate == DateTime.MinValue)
            {
                i_RecordDate = DateTime.Now.Date;
            }
            if (SecuritySystem.CurrentUser != null)
            {
                Employee currentEmpl = Session.FindObject<Employee>(new BinaryOperator("SysUser", SecuritySystem.CurrentUserId));
                if (currentEmpl != null)
                    i_Employee = currentEmpl;
            }
            // �������� ����� ��������� ���� ����� ����������� �� ������� ���� �����
            if (i_tomeNo == null || i_tomeNo == "")
            {
                IsogdBooksCard i_IsogdBooksCard = Session.FindObject<IsogdBooksCard>(CriteriaOperator.Parse(string.Format(
                            "IsogdBooksKind = '{0}' and ActiveTomeFlag = '{1}'", Enums.IsogdBooksKind.����������������, true)));
                if (i_IsogdBooksCard != null)
                {
                    if (i_IsogdBooksCard.BookTomeNo != null)
                    {
                        i_tomeNo = i_IsogdBooksCard.BookTomeNo;
                    }
                }
            }
        }
        protected override void OnSaving()
        {
            if (!IsDeleted)
            {
                try
                {
                    if (this.RecNoCore == String.Empty || this.RecNoCore == null)
                        this.RecNoCore = Convert.ToString(DistributedIdGeneratorHelper.Generate(this.Session.DataLayer, this.GetType().FullName, string.Empty));
                }
                catch { }
            }
            base.OnSaving();
        }
    }
}