using System;
using DevExpress.Xpo;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.Persistent.BaseImpl;
using ISOGDXAF.General;
using ISOGDXAF.OrgStructure;
using DevExpress.ExpressApp;
using DevExpress.Data.Filtering;
using ISOGDXAF.Enums;
using ISOGDXAF.Subject;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.Editors;

namespace ISOGDXAF.Isogd
{
    /// <summary>
    /// ������� ����� ������
    /// </summary>
    [NavigationItem("����� ����� �����"), System.ComponentModel.DefaultProperty("descript")]
    public class IsogdRequestCard : AttachBase
    {
        public IsogdRequestCard(Session session) : base(session) { }

        private DateTime i_RecordDate;
        private Employee i_Employee;
        private RequesterType i_requesterType;
        private Org i_org;
        private Subject.Person i_person;
        private Subject.Person i_TrustSubject;
        private ServiceStatus i_ServiceStatus;

        [Size(255), System.ComponentModel.Browsable(false)]
        public string descript
        {
            get
            {
                return String.Format("������� ����� ������ � {0} �� {1}", CardNo, RecordDate.ToShortDateString());
            }
        }

        private string i_tomeNo;
        [Size(4), DisplayName("����� ���� ����� ����� ������")]
        public string TomeNo
        {
            get { return i_tomeNo; }
            set { SetPropertyValue("TomeNo", ref i_tomeNo, value); }
        }

        [Persistent("CardNo")]
        private string RecNoCore;
        [PersistentAlias("RecNoCore")]
        [DisplayName("����� �������")]
        public string CardNo
        {
            get { return RecNoCore; }
        }

        [DisplayName("���� �������")]
        public DateTime RecordDate
        {
            get { return i_RecordDate; }
            set { SetPropertyValue("RecordDate", ref i_RecordDate, value); }
        }

        [DisplayName("��� ���������")]
        [ImmediatePostData]
        public RequesterType RequesterType
        {
            get { return i_requesterType; }
            set { SetPropertyValue("RequesterType", ref i_requesterType, value); }
        }

        [DisplayName("��������")]
        [Appearance("isOrg", Visibility = ViewItemVisibility.Hide, Criteria = "RequesterType= '���'", Context = "DetailView")]
        public Org RequesterOrg
        {
            get { return i_org; }
            set { SetPropertyValue("RequesterOrg", ref i_org, value); }
        }

        [DisplayName("��������")]
        [Appearance("isPerson", Visibility = ViewItemVisibility.Hide, Criteria = "RequesterType= '��'", Context = "DetailView")]
        public Subject.Person RequesterPerson
        {
            get { return i_person; }
            set { SetPropertyValue("RequesterPerson", ref i_person, value); }
        }

        [DisplayName("���������� ����")]
        [Appearance("isPerson2", Visibility = ViewItemVisibility.Hide, Criteria = "RequesterType= '���'", Context = "DetailView")]
        public Subject.Person TrustSubject
        {
            get { return i_TrustSubject; }
            set { SetPropertyValue("TrustSubject", ref i_TrustSubject, value); }
        }
         

        [DisplayName("������ ������")]
        [ImmediatePostData]
        public ServiceStatus ServiceStatus
        {
            get { return i_ServiceStatus; }
            set
            {
                try { SetPropertyValue("ServiceStatus", ref i_ServiceStatus, value); }
                catch { }
            }
        }

        [DisplayName("�����"), Size(16)]
        [Appearance("isMoney", Visibility = ViewItemVisibility.Hide, Criteria = "ServiceStatus= '���������'", Context = "DetailView")]
        public string MoneySum { get; set; }

        [DisplayName("���������, ������������� �����������")]
        public Employee Empl
        {
            get { return i_Employee; }
            set { SetPropertyValue("Empl", ref i_Employee, value); }
        }

        [Size(SizeAttribute.Unlimited), DisplayName("����������")]
        public string Notes { get; set; }

        [Association, DisplayName("������������� ��������� �����")]
        public XPCollection<IsogdDocument> IsogdDocument
        {
            get { return GetCollection<IsogdDocument>("IsogdDocument"); }
        }

        [Association, DisplayName("��������������� ������ � �������������� ��������")]
        public XPCollection<IsogdOutDataCard> IsogdOutDataCard
        {
            get { return GetCollection<IsogdOutDataCard>("IsogdOutDataCard"); }
        }
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
            if (i_RecordDate == DateTime.MinValue)
            {
                i_RecordDate = DateTime.Now.Date;
            }
            if (SecuritySystem.CurrentUser != null)
            {
                Employee currentEmpl = Session.FindObject<Employee>(new BinaryOperator("SysUser", SecuritySystem.CurrentUserId));
                if (currentEmpl != null)
                    i_Employee = currentEmpl;
            }
            // �������� ����� ��������� ���� ����� ����� ������ �� ������� ���� �����
            if (i_tomeNo == null || i_tomeNo == "")
            {
                IsogdBooksCard i_IsogdBooksCard = Session.FindObject<IsogdBooksCard>(CriteriaOperator.Parse(string.Format(
                            "IsogdBooksKind = '{0}' and ActiveTomeFlag = '{1}'", Enums.IsogdBooksKind.����������������, true)));
                if (i_IsogdBooksCard != null)
                {
                    if (i_IsogdBooksCard.BookTomeNo != null)
                    {
                        i_tomeNo = i_IsogdBooksCard.BookTomeNo;
                    }
                }
            }
        }
        protected override void OnSaving()
        {
            if (!IsDeleted)
            {
                try
                {
                    if (this.RecNoCore == String.Empty || this.RecNoCore == null)
                        this.RecNoCore = Convert.ToString(DistributedIdGeneratorHelper.Generate(this.Session.DataLayer, this.GetType().FullName, string.Empty));
                }
                catch { }
            }
            base.OnSaving();
        }
    }
}