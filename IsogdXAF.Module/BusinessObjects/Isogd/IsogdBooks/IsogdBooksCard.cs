using System;
using DevExpress.Xpo;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.Persistent.BaseImpl;
using ISOGDXAF.General;
using ISOGDXAF.Enums;
using ISOGDXAF.OrgStructure;
using DevExpress.ExpressApp;
using DevExpress.Data.Filtering;
using System.Collections;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.Editors;
using DevExpress.XtraEditors;

namespace ISOGDXAF.Isogd
{

    /// <summary>
    /// ������ ������� ���� �����
    /// </summary>
    [NavigationItem("����� ����� �����"), System.ComponentModel.DefaultProperty("descript")]
    public class IsogdBooksCard : BaseObject
    {
        public IsogdBooksCard(Session session) : base(session) { }

        private DateTime i_RecordDate;
        private DateTime i_OpenenigTomeDate;
        private DateTime i_ClosingTomeDate;
        private Employee i_Employee;
        private bool i_ActiveTomeFlag;


        [Size(255), System.ComponentModel.Browsable(false)]
        public string descript
        {
            get
            {
                return String.Format("������ ������� ���� ����� � {0} �� {1}", RecNo, RecordDate.ToShortDateString());
            }
        }

        [Persistent("RecNo")]
        private string RecNoCore;
        [PersistentAlias("RecNoCore")]
        [DisplayName("����� ������")]
        public string RecNo
        {
            get { return RecNoCore; }
        }

        [DisplayName("���� ������")]
        public DateTime RecordDate
        {
            get { return i_RecordDate; }
            set { SetPropertyValue("RecordDate", ref i_RecordDate, value); }
        }

        private IsogdBooksKind i_IsogdBooksKind;
        [DisplayName("��� �����")]
        [ImmediatePostData]
        public IsogdBooksKind IsogdBooksKind
        {
            get { return i_IsogdBooksKind; }
            set { SetPropertyValue("IsogdBooksKind", ref i_IsogdBooksKind, value); }
        }

        private string i_BookTomeNo;
        [DisplayName("����� ���� �����")]
        public string BookTomeNo
        {
            get { return i_BookTomeNo; }
            set { SetPropertyValue("BookTomeNo", ref i_BookTomeNo, value); }
        }

        // ���������� ����� ���� �����
        private string SetBookTomeNo()
        {
            string res = "";
            //XtraMessageBox.Show(IsogdBooksKind.ToString());
            if (IsogdBooksKind != Enums.IsogdBooksKind.�������������)
            {
                IsogdBooksCard ibc = Session.FindObject<IsogdBooksCard>(CriteriaOperator.Parse(string.Format("IsogdBooksKind = '{0}' and ActiveTomeFlag= 'true'", IsogdBooksKind)));
                if (ibc != null)
                {
                    if (ibc != this)
                    {
                        res = (Convert.ToInt16(ibc.BookTomeNo) + 1).ToString();
                        ibc.ClosingTomeDate = DateTime.Now.Date;
                        ibc.NextBookTomeNo = res;
                        ibc.ActiveTomeFlag = false;
                        this.ActiveTomeFlag = true;
                        ibc.Save();
                    }
                }
                else
                {
                    res = "1";
                    this.ActiveTomeFlag = true;
                }
            }
            // ��� ����� �������� ���� ��-�������
            else
            {
                if (IsogdStorageBook != null)
                {
                    IsogdBooksCard ibc = Session.FindObject<IsogdBooksCard>(CriteriaOperator.Parse(string.Format("IsogdStorageBook.Oid = '{0}' and ActiveTomeFlag= '{1}'", IsogdStorageBook.Oid, true)));
                    if (ibc != null)
                    {
                        if (ibc != this)
                        {
                            res = (Convert.ToInt16(ibc.BookTomeNo) + 1).ToString();
                            ibc.ClosingTomeDate = DateTime.Now.Date;
                            ibc.NextBookTomeNo = res;
                            ibc.ActiveTomeFlag = false;
                            this.ActiveTomeFlag = true;
                            ibc.Save();
                        }
                    }
                    else
                    {
                        res = "1";
                        this.ActiveTomeFlag = true;
                    }
                }
                else
                {
                    this.Notes = IsogdBooksKind.ToString();
                }
            }
            //res = (Session.GetObjects(Session.GetClassInfo<IsogdBooksCard>(),
            //    CriteriaOperator.Parse(string.Format("IsogdBooksKind = '{0}' and ActiveTomeFlag= 'true'", IsogdBooksKind)), null, 0, false, false).Count + 1).ToString();
            return res;
        }

        //private string i_StoreBookNo;
        //[DisplayName("��� ����� ��������")]
        //[Appearance("isStorBook", Visibility = ViewItemVisibility.Show, Criteria = "IsogdBooksKind= '�������������'", Context = "DetailView")]
        //public string StoreBookNo
        //{
        //    get { return i_StoreBookNo; }
        //    set { SetPropertyValue("StoreBookNo", ref i_StoreBookNo, value); }
        //}

        private IsogdStorageBook i_StoreBook;
        [DisplayName("����� ��������")]
        public IsogdStorageBook IsogdStorageBook
        {
            get { return i_StoreBook; }
            set { SetPropertyValue("IsogdStorageBook", ref i_StoreBook, value); }
        }

        [DisplayName("�������� ���")]
        public bool ActiveTomeFlag
        {
            get { return i_ActiveTomeFlag; }
            set { SetPropertyValue("ActiveTomeFlag", ref i_ActiveTomeFlag, value); }
        }
        [DisplayName("���� ������ ������� ���� �����")]
        public DateTime OpenenigTomeDate
        {
            get { return i_OpenenigTomeDate; }
            set { SetPropertyValue("OpenenigTomeDate", ref i_OpenenigTomeDate, value); }
        }
        [DisplayName("���� �������� ����")]
        public DateTime ClosingTomeDate
        {
            get { return i_ClosingTomeDate; }
            set { SetPropertyValue("ClosingTomeDate", ref i_ClosingTomeDate, value); }
        }
        [DisplayName("����� ���� ����� ����������� �� �������")]
        public string NextBookTomeNo { get; set; }

        [DisplayName("���������, ������������� ����")]
        public Employee Empl
        {
            get { return i_Employee; }
            set { SetPropertyValue("Empl", ref i_Employee, value); }
        }

        [Size(SizeAttribute.Unlimited), DisplayName("����������")]
        public string Notes { get; set; }

      
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
            if (i_RecordDate == DateTime.MinValue)
            {
                i_RecordDate = DateTime.Now.Date;
            }
            
            if (SecuritySystem.CurrentUser != null)
            {
                Employee currentEmpl = Session.FindObject<Employee>(new BinaryOperator("SysUser", SecuritySystem.CurrentUserId));
                if (currentEmpl != null)
                    i_Employee = currentEmpl;
            }
            if (i_OpenenigTomeDate == DateTime.MinValue)
            {
                i_OpenenigTomeDate = DateTime.Now.Date;
            }
            
        }

        protected override void OnSaving()
        {
            base.OnSaving();
            if (!IsDeleted)
            {
                //try
                //{
                    if (this.BookTomeNo == String.Empty || this.BookTomeNo == null)
                        this.BookTomeNo = SetBookTomeNo();
                    //this.Save();
                //}
                //catch { }
                try
                {
                    if (this.RecNoCore == String.Empty || this.RecNoCore == null)
                        this.RecNoCore = Convert.ToString(DistributedIdGeneratorHelper.Generate(this.Session.DataLayer, this.GetType().FullName, string.Empty));
                }
                catch { }
            }
        }
    }
}