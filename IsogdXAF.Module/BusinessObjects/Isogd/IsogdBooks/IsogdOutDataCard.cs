using System;
using DevExpress.Xpo;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.Persistent.BaseImpl;
using ISOGDXAF.General;
using ISOGDXAF.OrgStructure;
using DevExpress.ExpressApp;
using DevExpress.Data.Filtering;
using ISOGDXAF.Enums;
using ISOGDXAF.Subject;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.Editors;

namespace ISOGDXAF.Isogd
{
    /// <summary>
    /// ��������������� ������ � �������������� ��������
    /// </summary>
    [NavigationItem("����� ����� �����"), System.ComponentModel.DefaultProperty("descript")]
    public class IsogdOutDataCard : AttachBase
    {
        public IsogdOutDataCard(Session session) : base(session) { }

        private DateTime i_RecordDate;
        private Employee i_Employee;
        private ServiceStatus i_ServiceStatus;
        private DateTime i_PaymantDate;
        private string i_PaymentDoc;
        private IsogdReceivingMethod i_SendingMethod;
        private IsogdDocumentForm i_DataIssueForm;
        private DateTime i_DataCompleteDate;
        private bool i_DataCompleteFlag;
        private DateTime i_DataIssueDate;
        private bool i_DataIssueFlag;
        private Employee i_EmplDataIssue;
        private string i_TrustingDoc;
        private Subject.Person i_Person;
        IsogdRequestCard i_IsogdRequestCard;

        [Size(255), System.ComponentModel.Browsable(false)]
        public string descript
        {
            get
            {
                return String.Format("��������������� ������ � �������������� �������� ����� � {0} �� {1}", CardNo, RecordDate.ToShortDateString());
            }
        }


        [Association, DisplayName("������ �� �������������� ��������")]
        [ImmediatePostData]
        public IsogdRequestCard IsogdRequestCard
        {
            get { return i_IsogdRequestCard; }
            set { SetPropertyValue("IsogdRequestCard", ref i_IsogdRequestCard, value); }
        }

        private string i_tomeNo;
        [Size(4), DisplayName("����� ���� ����� �������������� ��������")]
        public string TomeNo
        {
            get { return i_tomeNo; }
            set { SetPropertyValue("TomeNo", ref i_tomeNo, value); }
        }

        [Persistent("CardNo")]
        private string RecNoCore;
        [PersistentAlias("RecNoCore")]
        [DisplayName("����� ������")]
        public string CardNo
        {
            get { return RecNoCore; }
        }
        

        [DisplayName("���� ������")]
        public DateTime RecordDate
        {
            get { return i_RecordDate; }
            set { SetPropertyValue("RecordDate", ref i_RecordDate, value); }
        }

        [DisplayName("������ ������")]
        [ImmediatePostData]
        public ServiceStatus ServiceStatus
        {
            get { return i_ServiceStatus; }
            set
            {
                try { SetPropertyValue("ServiceStatus", ref i_ServiceStatus, value); }
                catch { }
            }
        }

        private string i_MoneySum;
        [DisplayName("�����"), Size(16)]
        [Appearance("isMoney", Visibility = ViewItemVisibility.Hide, Criteria = "ServiceStatus= '���������'", Context = "DetailView")]
        public string MoneySum
        {
            get
            {
                if (i_MoneySum != null || i_MoneySum != "")
                    if (IsogdRequestCard != null)
                        if (IsogdRequestCard.MoneySum != null)
                            i_MoneySum = IsogdRequestCard.MoneySum;
                return i_MoneySum;
            }
            set { SetPropertyValue("MoneySum", ref i_MoneySum, value); }
        }

        [DisplayName("���� ������")]
        [Appearance("isMoney1", Visibility = ViewItemVisibility.Hide, Criteria = "ServiceStatus= '���������'", Context = "DetailView")]
        public DateTime PaymantDate
        {
            get { return i_PaymantDate; }
            set { SetPropertyValue("PaymantDate", ref i_PaymantDate, value); }
        }

        [Size(255), DisplayName("��������� ��������")]
        [Appearance("isMoney2", Visibility = ViewItemVisibility.Hide, Criteria = "ServiceStatus= '���������'", Context = "DetailView")]
        public string PaymentDoc
        {
            get { return i_PaymentDoc; }
            set { SetPropertyValue("PaymentDoc", ref i_PaymentDoc, value); }
        }

        [DisplayName("������ �������������� ��������")]
        public IsogdReceivingMethod SendingMethod
        {
            get { return i_SendingMethod; }
            set { SetPropertyValue("SendingMethod", ref i_SendingMethod, value); }
        }

        [DisplayName("����� �������������� ��������")]
        public IsogdDocumentForm DataIssueForm
        {
            get { return i_DataIssueForm; }
            set { SetPropertyValue("DataIssueForm", ref i_DataIssueForm, value); }
        }

        // ��� ����������� ����� � ���������� �������� ������������� ������������� ���� ���������� � ������� ���������
        [DisplayName("�������� ������������")]
        [ImmediatePostData]
        public bool DataCompleteFlag
        {
            get
            {
                return i_DataCompleteFlag;
            }
            set { SetPropertyValue("DataCompleteFlag", ref i_DataCompleteFlag, value); }
        }

        [DisplayName("���� ���������� ��������")]
        public DateTime DataCompleteDate
        {
            get
            {
                if (i_DataCompleteDate == DateTime.MinValue)
                        if (DataCompleteFlag == true)
                            i_DataCompleteDate = DateTime.Now.Date;
                return i_DataCompleteDate; }
            set { SetPropertyValue("DataCompleteDate", ref i_DataCompleteDate, value); }
        }

        [DisplayName("���������, ������������� ���������")]
        public Employee Empl
        {
            get {
                if (i_Employee == null)
                        if (DataCompleteFlag == true)
                        {
                            if (SecuritySystem.CurrentUser != null)
                            {
                                Employee currentEmpl = Session.FindObject<Employee>(new BinaryOperator("SysUser", SecuritySystem.CurrentUserId));
                                if (currentEmpl != null)
                                    i_Employee = currentEmpl;
                            }
                        }
                return i_Employee; }
            set { SetPropertyValue("Empl", ref i_Employee, value); }
        }


        [DisplayName("�������� ������")]
        [ImmediatePostData]
        public bool DataIssueFlag
        {
            get { return i_DataIssueFlag; }
            set { SetPropertyValue("DataIssueFlag", ref i_DataIssueFlag, value); }
        }

        [DisplayName("���� �������������� ��������")]
        public DateTime DataIssueDate
        {
            get {
                if (i_DataIssueDate == DateTime.MinValue)
                        if (DataIssueFlag == true)
                            i_DataIssueDate = DateTime.Now.Date;
                return i_DataIssueDate; }
            set { SetPropertyValue("DataIssueDate", ref i_DataIssueDate, value); }
        }

        [DisplayName("���������, �������� ���������")]
        public Employee EmplDataIssue
        {
            get {
                if (i_EmplDataIssue == null)
                        if (DataIssueFlag == true)
                        {
                            if (SecuritySystem.CurrentUser != null)
                            {
                                Employee currentEmpl = Session.FindObject<Employee>(new BinaryOperator("SysUser", SecuritySystem.CurrentUserId));
                                if (currentEmpl != null)
                                    i_EmplDataIssue = currentEmpl;
                            }
                        }
                return i_EmplDataIssue; }
            set { SetPropertyValue("EmplDataIssue", ref i_EmplDataIssue, value); }
        }

        

        
        [DisplayName("����, ���������� ��������")]
        public Subject.Person Person
        {
            get { return i_Person; }
            set { SetPropertyValue("Person", ref i_Person, value); }
        }

        [Size(255), DisplayName("������������")]
        public string TrustingDoc
        {
            get { return i_TrustingDoc; }
            set { SetPropertyValue("TrustingDoc", ref i_TrustingDoc, value); }
        }
        

        [Size(SizeAttribute.Unlimited), DisplayName("����������")]
        public string Notes { get; set; }

       
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
            if (i_RecordDate == DateTime.MinValue)
            {
                i_RecordDate = DateTime.Now.Date;
            }
            
            // �������� ����� ��������� ���� ����� ������ �������� �� ������� ���� �����
            if (i_tomeNo == null || i_tomeNo == "")
            {
                IsogdBooksCard i_IsogdBooksCard = Session.FindObject<IsogdBooksCard>(CriteriaOperator.Parse(string.Format(
                            "IsogdBooksKind = '{0}' and ActiveTomeFlag = '{1}'", Enums.IsogdBooksKind.���������������������������, true)));
                if (i_IsogdBooksCard != null)
                {
                    if (i_IsogdBooksCard.BookTomeNo != null)
                    {
                        i_tomeNo = i_IsogdBooksCard.BookTomeNo;
                    }
                }
            }
        }
        protected override void OnSaving()
        {
            if (!IsDeleted)
            {
                try
                {
                    if (this.RecNoCore == String.Empty || this.RecNoCore == null)
                        this.RecNoCore = Convert.ToString(DistributedIdGeneratorHelper.Generate(this.Session.DataLayer, this.GetType().FullName, string.Empty));
                }
                catch { }
            }
            base.OnSaving();
        }
    }
}