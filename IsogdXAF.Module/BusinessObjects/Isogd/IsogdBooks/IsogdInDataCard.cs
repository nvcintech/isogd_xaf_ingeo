using System;
using DevExpress.Xpo;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.Persistent.BaseImpl;
using ISOGDXAF.General;
using ISOGDXAF.OrgStructure;
using DevExpress.ExpressApp;
using DevExpress.Data.Filtering;

namespace ISOGDXAF.Isogd
{
    /// <summary>
    /// ������ ����� ����� ��������
    /// </summary>
    [NavigationItem("����� ����� �����"), System.ComponentModel.DefaultProperty("descript")]
    public class IsogdInDataCard : BaseObject
    {
        public IsogdInDataCard(Session session) : base(session) { }

        private DateTime i_RecordDate;
        private string i_docName;
        private IsogdDocumentNameKind i_docNameKind;
        private Employee i_Employee;
        private string i_tomeNo;

        [Size(255), System.ComponentModel.Browsable(false)]
        public string descript
        {
            get
            {
                return String.Format("������ ����� ����� �������� � {0} �� {1}", CardNo, RecordDate.ToShortDateString());
            }
        }

        [Size(4), DisplayName("����� ���� ����� ����� ��������")]
        public string TomeNo
        {
            get { return i_tomeNo; }
            set { SetPropertyValue("TomeNo", ref i_tomeNo, value); }
        }

        [Persistent("CardNo")]
        private string RecNoCore;
        [PersistentAlias("RecNoCore")]
        [DisplayName("����� ������")]
        public string CardNo
        {
            get { return RecNoCore; }
        }

        [DisplayName("���� ������")]
        public DateTime RecordDate
        {
            get { return i_RecordDate; }
            set { SetPropertyValue("RecordDate", ref i_RecordDate, value); }
        }

        [DisplayName("������ �������� ���������")]
        public IsogdReceivingMethod IsogdReceivingMethod { get; set; }

        [Size(64), DisplayName("����� ��������� � ������")]
        public string DocNo { get; set; }

        [DisplayName("������������ ��������� (���������)")]
        [ImmediatePostData]
        public IsogdDocumentNameKind IsogdDocumentNameKind
        {
            get { return i_docNameKind; }
            set
            {
                try { SetPropertyValue("IsogdDocumentNameKind", ref i_docNameKind, value); }
                catch { }
                OnChanged("IsogdDocumentNameKind");
            }
        }

        [Size(255), DisplayName("������������ ���������")]
        public string DocName
        {
            get
            {
                try
                {
                    {
                        if (i_docName == null || i_docName == "")
                            if (i_docNameKind.Name.Length > 0)
                                i_docName = i_docNameKind.Name;
                    }
                }
                catch { }
                return i_docName;
            }
            set
            {
                SetPropertyValue("DocName", ref i_docName, value);
            }
        }

        [DisplayName("����� ������������� ���������")]
        public IsogdDocumentForm IsogdDocumentForm { get; set; }

        [DisplayName("����� ���������"), Size(16)]
        public string DocumentVolume { get; set; }

        [DisplayName("��� ��������")]
        public IsogdDataStorageKind IsogdDataStorageKind { get; set; }

        [DisplayName("���������������� ������")]
        public IsogdLetter IsogdLetter { get; set; }

        [DisplayName("���������, ������������� ����")]
        public Employee Empl
        {
            get { return i_Employee; }
            set { SetPropertyValue("Empl", ref i_Employee, value); }
        }

        [DisplayName("�������� �����")]
        public IsogdDocument IsogdDocument { get; set; }

        [Association, DisplayName("�������� ����������� ��������� �����")]
        public IsogdRegistrationCard IsogdRegistrationCard { get; set; }

        [Size(SizeAttribute.Unlimited), DisplayName("����������")]
        public string Notes { get; set; }

       
        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
            if (i_RecordDate == DateTime.MinValue)
            {
                i_RecordDate = DateTime.Now.Date;
            }
            if (SecuritySystem.CurrentUser != null)
            {
                Employee currentEmpl = Session.FindObject<Employee>(new BinaryOperator("SysUser", SecuritySystem.CurrentUserId));
                if (currentEmpl != null)
                    i_Employee = currentEmpl;
            }
            // �������� ����� ��������� ���� ����� ����� �� ������� ���� �����
            if (i_tomeNo == null || i_tomeNo == "")
            {
                IsogdBooksCard i_IsogdBooksCard = Session.FindObject<IsogdBooksCard>(CriteriaOperator.Parse(string.Format(
                            "IsogdBooksKind = '{0}' and ActiveTomeFlag = '{1}'", Enums.IsogdBooksKind.������������������, true)));
                if (i_IsogdBooksCard != null)
                {
                    if (i_IsogdBooksCard.BookTomeNo != null)
                    {
                        i_tomeNo = i_IsogdBooksCard.BookTomeNo;
                    }
                }
            }
        }
        protected override void OnSaving()
        {
            if (!IsDeleted)
            {
                try
                {
                    if (this.RecNoCore == String.Empty || this.RecNoCore == null)
                        this.RecNoCore = Convert.ToString(DistributedIdGeneratorHelper.Generate(this.Session.DataLayer, this.GetType().FullName, string.Empty));
                }
                catch { }
            }
            base.OnSaving();
        }
    }
}