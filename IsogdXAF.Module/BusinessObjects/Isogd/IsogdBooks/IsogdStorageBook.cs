using System;
using DevExpress.Xpo;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.Persistent.BaseImpl;
using ISOGDXAF.General;
using ISOGDXAF.Land;

namespace ISOGDXAF.Isogd
{
    /// <summary>
    /// ����� �������� �������� �����
    /// </summary>
    [NavigationItem("������� � ��������� �����"), System.ComponentModel.DefaultProperty("descript")]
    public class IsogdStorageBook : BaseObject
    {
        public IsogdStorageBook(Session session) : base(session) { }

        private string i_name;

        [Size(255), System.ComponentModel.Browsable(false)]
        public string descript
        {
            get
            {
                return String.Format("����� �������� �������� ����� � {0}", Code);
            }
        }

        [Association, DisplayName("������ �����")]
        public IsogdPartition IsogdPartition { get; set; }

        [Size(64), DisplayName("��� ����� ��������")]
        public string Code { get; set; }


        [Size(255), DisplayName("������������ ����� ��������")]
        public string Name
        {
            get
            {
                return i_name;
            }
            set
            {
                SetPropertyValue("Name", ref i_name, value);
            }
        }

        

        [Size(SizeAttribute.Unlimited), DisplayName("����������")]
        public string Notes { get; set; }

        [Association("IsogdDocument-IsogdStorageBook"), DisplayName("��������� �����")]
        public XPCollection<IsogdDocument> IsogdDocument
        {
            get { return GetCollection<IsogdDocument>("IsogdDocument"); }
        }

        [Association, DisplayName("��������� �������, ��������� � ������")]
        public XPCollection<Lot> Lot
        {
            get { return GetCollection<Lot>("Lot"); }
        }
        [Association, DisplayName("���� ����� ��������")]
        public XPCollection<IsogdStorageBookTome> IsogdStorageBookTome
        {
            get { return GetCollection<IsogdStorageBookTome>("IsogdStorageBookTome"); }
        }
    }
}