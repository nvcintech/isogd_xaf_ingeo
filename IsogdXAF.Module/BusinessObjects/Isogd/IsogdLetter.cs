using System;
using DevExpress.Xpo;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.Persistent.BaseImpl;
using ISOGDXAF.Enums;
using DevExpress.ExpressApp.ConditionalAppearance;
using DevExpress.ExpressApp.Editors;
using ISOGDXAF.Subject;
using ISOGDXAF.General;

namespace ISOGDXAF.Isogd
{
    [NavigationItem("������� � ��������� �����"), System.ComponentModel.DefaultProperty("descript")]
    public class IsogdLetter : AttachBase
    {
        public IsogdLetter(Session session) : base(session) { }

        private ReasonKind i_ReasonKind;
        private DateTime i_OutDate;
        private DateTime i_InDate;
        private RequesterType i_requesterType;
        private Org i_org;
        private Subject.Person i_person;

        [Size(255), System.ComponentModel.Browsable(false)]
        public string descript
        {
            get
            {
                return String.Format("���������������� ������ � {0} �� {1}", InNo, InDate.ToShortDateString());
            }
        }
        [DisplayName("��� ���������")]
        [ImmediatePostData]
        public ReasonKind ReasonKind
        {
            get { return i_ReasonKind; }
            set { SetPropertyValue("ReasonKind", ref i_ReasonKind, value); }
        }

        [Size(64), DisplayName("��������� �����")]
        public string OutNo { get; set; }

        [DisplayName("���� �����������")]
        public DateTime OutDate
        {
            get { return i_OutDate; }
            set { SetPropertyValue("OutDate", ref i_OutDate, value); }
        }


        [Size(64), DisplayName("�������� - ���������")]
        [Appearance("isDoc", Visibility = ViewItemVisibility.Hide, Criteria = "ReasonKind= '����������������������'", Context = "DetailView")]
        public string ReasonDoc { get; set; }

        [Size(64), DisplayName("�������� �����")]
        public string InNo { get; set; }

        [DisplayName("���� ���������")]
        public DateTime InDate
        {
            get { return i_InDate; }
            set { SetPropertyValue("InDate", ref i_InDate, value); }
        }
        [DisplayName("��� ���������")]
        [Appearance("isLetter", Visibility = ViewItemVisibility.Hide, Criteria = "ReasonKind= '�����������������'", Context = "DetailView")]
        [ImmediatePostData]
        public RequesterType RequesterType
        {
            get { return i_requesterType; }
            set { SetPropertyValue("RequesterType", ref i_requesterType, value); }
        }
        [DisplayName("��������")]
        [Appearance("isLetter1", Visibility = ViewItemVisibility.Hide, Criteria = "ReasonKind= '�����������������'", Context = "DetailView")]
        [Appearance("isOrg", Visibility = ViewItemVisibility.Hide, Criteria = "RequesterType= '���'", Context = "DetailView")]
        public Org RequesterOrg
        {
            get { return i_org; }
            set { SetPropertyValue("RequesterOrg", ref i_org, value); }
        }

        [DisplayName("��������")]
        [Appearance("isLetter2", Visibility = ViewItemVisibility.Hide, Criteria = "ReasonKind= '�����������������'", Context = "DetailView")]
        [Appearance("isPerson", Visibility = ViewItemVisibility.Hide, Criteria = "RequesterType= '��'", Context = "DetailView")]
        public Subject.Person RequesterPerson
        {
            get { return i_person; }
            set { SetPropertyValue("RequesterPerson", ref i_person, value); }
        }

        [Size(SizeAttribute.Unlimited), DisplayName("����������")]
        public string Notes { get; set; }

        //public IsogdDocument IsogdDocument { get; set; }
        //[Association, DisplayName("��������� �����")]
        //public XPCollection<IsogdDocument> IsogdDocument
        //{
        //    get { return GetCollection<IsogdDocument>("IsogdDocument"); }
        //}

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            // Place here your initialization code.
        }
    }
}