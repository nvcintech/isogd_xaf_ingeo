using System;
using DevExpress.Xpo;
using DevExpress.Persistent.Base;
using System.Collections.Generic;
using DevExpress.Persistent.BaseImpl;
using ISOGDXAF.General;

namespace ISOGDXAF.Isogd
{
    public class AttachmentBase : BaseObject
    {
        public AttachmentBase(Session session) : base(session) { }
        [Aggregated, Association("IsogdDocumentLogic-AttachmentFiles"), DisplayName("����������� �����")]
        [FileTypeFilter("�����", 1, "*.doc", "*.docx", "*.xls", "*.xlsx", "*.txt")]
        [FileTypeFilter("�����������", 2, "*.bmp", "*.png", "*.gif", "*.jpg")]
        public XPCollection<AttachmentFiles> AttachmentFiles
        {
            get { return GetCollection<AttachmentFiles>("AttachmentFiles"); }
        }
    }

}