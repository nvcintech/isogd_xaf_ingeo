﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ISOGDXAF.SystemDir
{
    public class ExpressionTranslator
    {
        const string argumentKey = "<**##>";

        public static bool IsArgument(string argument)
        {
            return argument.Contains(argumentKey);
        }

        public static string GetReflectString(string argument)
        {
            return argument.Replace(argumentKey, "");
        }

        public static string Argument(string argument)
        {
            return argumentKey + argument + argumentKey;
        }
    }

    public class ExpressionTranslator<T>
    {
        public ExpressionTranslator(Expression<Func<T, bool>> expression)
        {
            if (expression.Body == null || !(expression.Body is BinaryExpression))
                throw new FormatException();

            var binaryExpression = expression.Body as BinaryExpression;
            expressionList = new List<object>();
            expressionList.Add(binaryExpression.Left);
            expressionList.Add(getLogicOperatorAlias(binaryExpression.NodeType));
            expressionList.Add(binaryExpression.Right);
        }

        List<object> expressionList;

        public string GetExpressionString()
        {
            translateLogicOperators();
            translateMethods();
            translateObjectMembers();
            translateConstants();

            string result = string.Empty;
            foreach (var s in expressionList)
            {
                if (s is Expression)
                    throw new Exception("Трансляция выполнена не полностью");
                result += s;
            }
            return result;
        }

        //------------------------------------------------------------------------------------------

        string getLogicOperatorAlias(ExpressionType expressionType)
        {
            switch (expressionType)
            {
                case ExpressionType.AndAlso: return " and ";
                case ExpressionType.OrElse: return " or ";
                case ExpressionType.Equal: return " = ";
                case ExpressionType.NotEqual: return " != ";
                case ExpressionType.GreaterThan: return " > ";
                case ExpressionType.GreaterThanOrEqual: return " >= ";
                case ExpressionType.LessThan: return " < ";
                case ExpressionType.LessThanOrEqual: return " <= ";
                default: throw new FormatException(string.Format("Такой тип команд не поддерживается ({0})", expressionType));
            }
        }

        bool isAndOrOperator(ExpressionType expressionType)
        {
            return expressionType == ExpressionType.AndAlso || expressionType == ExpressionType.OrElse;
        }

        string getReflectString(object value)
        {
            if (value == null)
                return "null";
            if (value is string)
            {
                var result = value.ToString();
                if (ExpressionTranslator.IsArgument(result))
                    return ExpressionTranslator.GetReflectString(result);
                return "'" + result + "'";
            }
            return value.ToString();
        }

        void translateLogicOperators()
        {
            bool isExpressionExist = true;
            while (isExpressionExist)
            {
                isExpressionExist = false;
                for (int i = 0; i < expressionList.Count; i++)
                {
                    if (expressionList[i] is BinaryExpression)
                    {
                        isExpressionExist = true;
                        var expr = expressionList[i] as BinaryExpression;
                        expressionList.RemoveAt(i);
                        if (isAndOrOperator(expr.NodeType))
                        {
                            expressionList.Insert(i, ")");
                            expressionList.Insert(i, "(");
                            i++;
                        }
                        expressionList.Insert(i, expr.Right);
                        expressionList.Insert(i, getLogicOperatorAlias(expr.NodeType));
                        expressionList.Insert(i, expr.Left);
                        break;
                    }
                }
            }
        }

        void translateMethods()
        {
            bool isExpressionExist = true;
            while (isExpressionExist)
            {
                isExpressionExist = false;
                for (int i = 0; i < expressionList.Count; i++)
                {
                    if (expressionList[i] is MethodCallExpression)
                    {
                        isExpressionExist = true;
                        var expr = expressionList[i] as MethodCallExpression;
                        expressionList.RemoveAt(i);
                        if (expr.Object != null && expr.Object.NodeType == ExpressionType.Parameter)
                            expressionList.Insert(i, expr.ToString());
                        else
                        {
                            var value = Expression.Lambda(expr).Compile().DynamicInvoke();
                            expressionList.Insert(i, getReflectString(value));
                        }
                        break;
                    }
                }
            }
        }

        void translateObjectMembers()
        {
            bool isExpressionExist = true;
            while (isExpressionExist)
            {
                isExpressionExist = false;
                for (int i = 0; i < expressionList.Count; i++)
                {
                    if (expressionList[i] is MemberExpression)
                    {
                        isExpressionExist = true;
                        var expr = expressionList[i] as MemberExpression;
                        expressionList.RemoveAt(i);
                        if (expr.Expression != null && expr.Expression.NodeType == ExpressionType.Parameter)
                        {
                            expressionList.Insert(i, expr.Member.Name);
                        }
                        else
                        {
                            try
                            {
                                var value = Expression.Lambda(expr).Compile().DynamicInvoke();
                                expressionList.Insert(i, getReflectString(value));
                            }
                            catch
                            {
                                expressionList.Insert(i, expr.Member.Name);
                                Debug.WriteLine("-----------------------------------------------");
                                Debug.WriteLine("--ExpressionTranslator-------------------------");
                                Debug.WriteLine("-----------------------------------------------");
                            }
                        }
                        break;
                    }
                }
            }
        }

        void translateConstants()
        {
            bool isExpressionExist = true;
            while (isExpressionExist)
            {
                isExpressionExist = false;
                for (int i = 0; i < expressionList.Count; i++)
                {
                    if (expressionList[i] is ConstantExpression)
                    {
                        isExpressionExist = true;
                        var expr = expressionList[i] as ConstantExpression;
                        expressionList.RemoveAt(i);
                        expressionList.Insert(i, getReflectString(expr.Value));
                        break;
                    }
                }
            }
        }
    }
}
