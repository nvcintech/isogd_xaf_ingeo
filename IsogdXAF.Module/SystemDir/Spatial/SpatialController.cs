﻿using DevExpress.Xpo;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ISOGDXAF.SystemDir
{
    public class SpatialController<TLayer>
        where TLayer : ISpatialLayer
    {
        public SpatialController(Connect connect)
        {
            this.connect = connect;
        }

        Connect connect;
        //const string layerPattern = @"/arcgis/rest/services/";

        public SpatialLayerItem GetSpatialLayerItem(string isogdClassName, string spatialLayerId)
        {
            //if (!spatialLayerId.StartsWith(layerPattern))
            //    throw new ArgumentException(string.Format("Неверный формат слоя ({0})", spatialLayerId));
            if (string.IsNullOrEmpty(isogdClassName))
                throw new FormatException("IsogdClassName is empty");
            var confObj = connect.FindFirstObject<SpatialConfig>(mc => mc.IsogdClassName == isogdClassName);
            return confObj.Layers.SingleOrDefault(mc => mc.SpatialLayer.Id.Equals(spatialLayerId));
        }
        /// <summary>
        /// Получение связанных ИСОГД-классов из конфига
        /// </summary>
        /// <param name="spatialLayerId">ID пространственного слоя</param>
        /// <returns></returns>
        public IEnumerable<string> GetLinkedIsogdClasses(string spatialLayerId)
        {
            if (string.IsNullOrEmpty(spatialLayerId))
                throw new FormatException("LayerUrl is empty");
            return from cl in connect.Query<SpatialConfig>()
                   where cl.Layers.Any(mc => mc.SpatialLayer.Id == spatialLayerId)
                   select cl.IsogdClassName;
        }
        /// <summary>
        /// Получение пространственных слоев доступных для связи
        /// </summary>
        /// <param name="isogdClassName">ИСОГД-класс</param>
        /// <returns></returns>
        public IEnumerable<string> GetLinkedSpatialLayers(string isogdClassName)
        {
            if (string.IsNullOrEmpty(isogdClassName))
                throw new FormatException("IsogdClassName is empty");
            return from layer in connect.FindFirstObject<SpatialConfig>(
                      mc => mc.IsogdClassName == isogdClassName).Layers
                   //where !layer.IsViewOnly
                   select layer.SpatialLayer.Id;
        }
        /// <summary>
        /// Получение пространственных слоев доступных только для просмотра
        /// </summary>
        /// <param name="isogdClassName">ИСОГД-класс</param>
        /// <returns></returns>
        public IEnumerable<string> GetViewOnlySpatialLayers(string isogdClassName)
        {
            if (string.IsNullOrEmpty(isogdClassName))
                throw new FormatException("IsogdClassName is empty");
            return from layer in connect.FindFirstObject<SpatialConfig>(
                      mc => mc.IsogdClassName == isogdClassName).Layers
                   //where layer.IsViewOnly
                   select layer.SpatialLayer.Id;
        }
        /// <summary>
        /// Проверяет возможно ли связать объекты данного пространственного слоя
        /// </summary>
        /// <param name="isogdClassName">ИСОГД-класс</param>
        /// <param name="spatialLayerId">ID пространственного слоя</param>
        /// <returns></returns>
        public bool IsLayerValid(string isogdClassName, string spatialLayerId)
        {
            return connect.FindFirstObject<SpatialConfig>(
                mc => mc.IsogdClassName == isogdClassName).Layers.Any(
                mc => mc.SpatialLayer.Id == spatialLayerId);
            //mc => !mc.IsViewOnly && mc.SpatialLayer.Id == spatialLayerId);
        }

        //---------------------------------------------------------------------------------

        /// <summary>
        /// Проверяет есть ли у реестровго объекта связанный пространственный объект(ы)
        /// </summary>
        /// <param name="isogdClassName">ИСОГД-класс</param>
        /// <param name="isogdObjectId">ID реестрового объекта</param>
        /// <returns></returns>
        public bool IsObjectLinked(string isogdClassName, string isogdObjectId)
        {
            if (string.IsNullOrEmpty(isogdClassName))
                throw new FormatException("IsogdClassName is empty");
            if (string.IsNullOrEmpty(isogdObjectId))
                throw new FormatException("IsogdObjectId is empty");
            return connect.IsExist<SpatialRepository>(mc => mc.IsogdClassName == isogdClassName &&
                mc.IsogdObjectId == isogdObjectId);
        }
        /// <summary>
        /// Добавляет к реестровому объекту ссылку на пространственный объект. Проверка на уже существующую связь не осуществляется.
        /// </summary>
        /// <param name="isogdClassName">ИСОГД-класс</param>
        /// <param name="isogdObjectId">ID реестрового объекта</param>
        /// <param name="spatialLayerId">ID пространственного слоя</param>
        /// <param name="spatialObjectId">ID пространственного объекта</param>
        public void AddSpatialLink(string isogdClassName, string isogdObjectId,
           string spatialLayerId, string spatialObjectId)
        {
            if (string.IsNullOrEmpty(isogdClassName))
                throw new FormatException("IsogdClassName is empty");
            if (string.IsNullOrEmpty(isogdObjectId))
                throw new FormatException("IsogdObjectId is empty");
            if (!IsLayerValid(isogdClassName, spatialLayerId))
                throw new Exception("Incorrect spatial layer ");
            else
            {
                var repoObject = connect.CreateObject<SpatialRepository>();
                repoObject.IsogdClassName = isogdClassName;
                repoObject.IsogdObjectId = isogdObjectId;
                repoObject.SpatialLayerId = spatialLayerId;
                repoObject.SpatialObjectId = spatialObjectId;
                connect.SaveObject(repoObject);
            }
        }
        /// <summary>
        /// Получает все связанные пространственные объекты
        /// </summary>
        /// <param name="isogdClassName">ИСОГД-класс</param>
        /// <param name="isogdObjectId">ID реестрового объекта</param>
        /// <returns></returns>
        public IQueryable<SpatialRepository> GetSpatialLinks(string isogdClassName, string isogdObjectId)
        {
            return connect.FindObjects<SpatialRepository>(mc => mc.IsogdClassName == isogdClassName &&
                mc.IsogdObjectId == isogdObjectId);
        }
        /// <summary>
        /// Удаляет все связанные пространственные объекты. Удаление определенной связи не предусмотрено (можно через xaf-класс).
        /// </summary>
        /// <param name="isogdClassName">ИСОГД-класс</param>
        /// <param name="isogdObjectId">ID реестрового объекта</param>
        public void DeleteSpatialLinks(string isogdClassName, string isogdObjectId)
        {
            if (string.IsNullOrEmpty(isogdClassName))
                throw new FormatException("IsogdClassName is empty");
            if (string.IsNullOrEmpty(isogdObjectId))
                throw new FormatException("IsogdObjectId is empty");
            if (!IsObjectLinked(isogdClassName, isogdObjectId))
                throw new Exception("Link is not found");
            foreach (var link in GetSpatialLinks(isogdClassName, isogdObjectId))
            {
                connect.DeleteObject(link);
            }
        }
        /// <summary>
        /// Получает все связанные реестровые объекты
        /// </summary>
        /// <param name="spatialLayerId">ID пространственного слоя</param>
        /// <param name="spatialObjectId">ID пространственного объекта</param>
        /// <returns></returns>
        public IQueryable<SpatialRepository> GetIsogdLinks(string spatialLayerId, string spatialObjectId)
        {
            if (string.IsNullOrEmpty(spatialLayerId))
                throw new FormatException("LayerUrl is empty");
            if (string.IsNullOrEmpty(spatialObjectId))
                throw new FormatException("ObjectId is empty");
            return connect.FindObjects<SpatialRepository>(mc => mc.SpatialLayerId == spatialLayerId &&
                mc.SpatialObjectId == spatialObjectId);
        }
    }
}
