﻿using DevExpress.Xpo;
using Ingeo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace IsogdXAF.SystemDir.Ingeo
{
    public class IngeoSpatialFunctions
    {
        [DllImport("oleaut32.dll")]
        private static extern long GetActiveObject(ref Guid rclsid, IntPtr pvResreved, out IntPtr ppunk);

        /// <summary>
        /// Получить активную Ингео
        /// </summary>
        /// <returns></returns>
        public IngeoApplication GetActiveIngeo()
        {
            IntPtr ppunk;
            var rclsid = new Guid("{04088492-0485-11D4-9719-000021C6D845}");
            GetActiveObject(ref rclsid, IntPtr.Zero, out ppunk);
            if (ppunk.ToInt32() != 0)
                return Marshal.GetObjectForIUnknown(ppunk) as IngeoApplication;
            else
                return null;
        }


    }
}
