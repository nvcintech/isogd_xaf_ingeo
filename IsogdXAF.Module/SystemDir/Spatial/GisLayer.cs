﻿using DevExpress.Data.Filtering;
using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISOGDXAF.SystemDir
{
    [Custom("Caption", "ГИС слой"), NavigationItem("Настройки пространственных связей")]
    [System.ComponentModel.DefaultProperty("Id")]
    public class GisLayer : XPLiteObject, ISpatialLayer
    {
        public GisLayer() : base() { }
        public GisLayer(Session session) : base(session) { }

        private string _id;
        [Key(false), DisplayName("ID слоя"), VisibleInListView(true), Size(128)]
        public string Id
        {
            get { return _id; }
            set { SetPropertyValue("Id", ref _id, value); }
        }

        private string i_name;
        [DisplayName("Наименование слоя"), VisibleInListView(true), Size(255)]
        public string Name
        {
            get { return i_name; }
            set { SetPropertyValue("Name", ref i_name, value); }
        }

        public override string ToString()
        {
            return this.Id;
        }

        protected override void OnSaving()
        {
            if (Connect.FromSession(Session).IsExist<GisLayer>(mc => mc.Id == this.Id))
                throw new Exception("Такой слой уже существует");
            base.OnSaving();
        }
    }
}
