﻿using DevExpress.Xpo;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Base;
using System;
using System.ComponentModel;

namespace ISOGDXAF.SystemDir
{
    [Custom("Caption", "Настройки пространственных связей"), NavigationItem("Настройки пространственных связей")]
    [System.ComponentModel.DefaultProperty("IsogdClassName")]
    public class SpatialConfig : XPLiteObject
    {
        public SpatialConfig() : base() { }
        public SpatialConfig(Session session) : base(session) {}

        private Guid _id;
        [Browsable(false)]
        [Key(true)]
        public Guid ID
        {
            get { return _id; }
            set { SetPropertyValue("ID", ref _id, value); }
        }

        private string _isogdClassName;
        [DevExpress.Xpo.DisplayName("Класс ИСОГД")]
        public string IsogdClassName
        {
            get { return _isogdClassName; }
            set { SetPropertyValue("IsogdClassName", ref _isogdClassName, value); }
        } 

        [Aggregated, Association, DevExpress.Xpo.DisplayName("Слои")]
        public XPCollection<SpatialLayerItem> Layers
        {
            get { return GetCollection<SpatialLayerItem>("Layers"); }
        }

        protected override void OnSaving()
        {
            sortLayers();
            base.OnSaving();
        }

        void sortLayers()
        {
            int i = 0;
            Layers.Sorting = new SortingCollection(new [] { new SortProperty("Position", DevExpress.Xpo.DB.SortingDirection.Ascending) });
            foreach (var item in Layers)
            {
                item.Position = i;
                i++;
                item.Save();
                Session.Save(item);
            }
        }
    }
}