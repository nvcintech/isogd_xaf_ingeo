﻿using DevExpress.Persistent.Base;
using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISOGDXAF.SystemDir
{
    public class SpatialLayerItem : XPLiteObject
    {
        public SpatialLayerItem() : base() { }
        public SpatialLayerItem(Session session) : base(session) { }

        private Guid _id;
        [Browsable(false), Key(true)]
        public Guid ID
        {
            get { return _id; }
            set { SetPropertyValue("ID", ref _id, value); }
        }

        private GisLayer _spatialLayer;
        [DevExpress.Xpo.DisplayName("Слой")]
        [Indexed(Unique = true)]
        public GisLayer SpatialLayer
        {
            get { return _spatialLayer; }
            set { SetPropertyValue("SpatialLayer", ref _spatialLayer, value); }
        }

        private int _position;
        [Browsable(false)]
        public int Position
        {
            get { return _position; }
            set { SetPropertyValue("Position", ref _position, value); }
        }

        //private bool _isViewOnly;
        //[DevExpress.Xpo.DisplayName("Слой только для показа")]
        //public bool IsViewOnly
        //{
        //    get { return _isViewOnly; }
        //    set { SetPropertyValue("IsViewOnly", ref _isViewOnly, value); }
        //}

        private SpatialConfig _spatialConfig;
        [Association, Browsable(false)]
        public SpatialConfig SpatialConfig
        {
            get { return _spatialConfig; }
            set { SetPropertyValue("SpatialConfig", ref _spatialConfig, value); }
        }

        const int defaultPosition = int.MaxValue;

        public override void AfterConstruction()
        {
            base.AfterConstruction();
            //this.IsViewOnly = false;
            this.Position = defaultPosition;
        }
    }
}
