﻿using DevExpress.Xpo;
using DevExpress.Persistent.BaseImpl;
using DevExpress.Persistent.Base;
using System;
using DevExpress.ExpressApp;
using DevExpress.Data.Filtering;

namespace ISOGDXAF.SystemDir
{
    [Custom("Caption", "Связь реестровых и пространственных данных"), NavigationItem("Настройки пространственных связей")]
    [System.ComponentModel.DefaultProperty("IsogdClassName")]
    public class SpatialRepository : XPLiteObject
    {
        public SpatialRepository() : base() { }
        public SpatialRepository(Session session) : base(session) { }

        private Guid _id;
        [Key(true)]
        public Guid ID
        {
            get { return _id; }
            set { SetPropertyValue("ID", ref _id, value); }
        }

        private string _isogdClassName;
        [DisplayName("Класс реестрового объекта")]
        public string IsogdClassName
        {
            get { return _isogdClassName; }
            set { SetPropertyValue("IsogdClassName", ref _isogdClassName, value); }
        }

        private string _isogdObjectId;
        [DisplayName("ID реестрового объекта")]
        public string IsogdObjectId
        {
            get { return _isogdObjectId; }
            set { SetPropertyValue("IsogdObjectId", ref _isogdObjectId, value); }
        }

        private string _spatialLayerId;
        [DisplayName("Слой пространственного объекта")]
        public string SpatialLayerId
        {
            get { return _spatialLayerId; }
            set { SetPropertyValue("SpatialLayerId", ref _spatialLayerId, value); }
        }

        private string _spatialObjectId;
        [DisplayName("ID пространственного объекта")]
        public string SpatialObjectId
        {
            get { return _spatialObjectId; }
            set { SetPropertyValue("SpatialObjectId", ref _spatialObjectId, value); }
        }
    }
}