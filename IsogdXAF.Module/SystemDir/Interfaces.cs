﻿using DevExpress.Xpo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ISOGDXAF.SystemDir
{
    public interface ISpatialLayer
    {
        string Id { get; set; }
    }
}
