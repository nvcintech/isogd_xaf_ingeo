﻿using DevExpress.Xpo.DB;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Xml.Linq;

namespace ISOGDXAF.SystemDir
{
    public static class Settings
    {
        private static bool isLoaded = false;

        public static void Create()
        {
            if (!isLoaded)
            {
                try
                {
                    //var request = (HttpWebRequest)WebRequest.Create("http://" + HttpContext.Current.Request.Url.Authority + "/isogd-config.xml");
                    //var response = request.GetResponse();
                    //var doc = XDocument.Load(response.GetResponseStream());
                    string assemblyFile = 
                        (new System.Uri(Assembly.GetExecutingAssembly().CodeBase)
                        ).LocalPath;
                    //(new System.Uri(Assembly.GetExecutingAssembly().CodeBase)
                    //    ).LocalPath.Replace("IsogdXAF.Module.dll", "");
                    var doc = XDocument.Load(Path.Combine(assemblyFile, "isogd-config.xml"));

                    foreach (var prop in typeof(Settings).GetProperties(BindingFlags.Public | BindingFlags.Static))
                    {
                        var node = doc.Root.Descendants().Where(mc => mc.Name.ToString() == prop.Name).SingleOrDefault();
                        if (node != null)
                        {
                            try
                            {
                                var value = TypeDescriptor.GetConverter(prop.PropertyType).ConvertFrom(node.Value);
                                prop.SetValue(null, value);
                            }
                            catch { }
                        }
                    }

                    isLoaded = true;
                }
                catch { }
            }
        }

        //для добавления настройки просто добавьте свойство в класс (должно быть открытым статическим)
        //тип данных св-ва любой, приведение автоматическое (если возможно)
        public static string DbServer { get; private set; }
        public static string DbLogin { get; private set; }
        public static string DbPassword { get; private set; }
        public static string DbName { get; private set; }
        //public static string ArcGisHostName { get; private set; }

        public static string GetDbConnectionString()
        {
            return MSSqlConnectionProvider.GetConnectionString(DbServer, DbLogin, DbPassword.ToString(), DbName);
        }
    }
}
