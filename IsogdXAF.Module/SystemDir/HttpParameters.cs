﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace ISOGDXAF.SystemDir
{
    public class HttpParameters
    {
        public HttpParameters(HttpContext context)
        {
            if (context.Request.HttpMethod == "POST")
            {
                Stream bodyStream = context.Request.InputStream;
                var encoding = context.Request.ContentEncoding;
                var reader = new StreamReader(bodyStream, encoding);
                parameters = HttpUtility.ParseQueryString(reader.ReadToEnd(), encoding);
            }
            else
                parameters = context.Request.Params;
        }

        NameValueCollection parameters;

        public string GetParameter(string paramName)
        {
            return parameters[paramName];
        }

        public string this[string paramName]
        {
            get
            {
                return GetParameter(paramName);
            }
        }
    }
}
