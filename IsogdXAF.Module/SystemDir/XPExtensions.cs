﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DevExpress.Xpo
{
    public static class XPExtensions
    {
        public static string GetClassName(this XPBaseObject xpBaseObject)
        {
            return xpBaseObject.GetType().Name;
        }
    }
}
