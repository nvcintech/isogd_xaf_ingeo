﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace ISOGDXAF.SystemDir
{
    public class Json
    {
        public static string Serialize(object serializeObject)
        {
            return new JavaScriptSerializer().Serialize(serializeObject);
        }

        public static T Deserialize<T>(string jsonString)
        {
            return new JavaScriptSerializer().Deserialize<T>(jsonString);
        }

        public static object Deserialize(string jsonString)
        {
            return new JavaScriptSerializer().DeserializeObject(jsonString);
        }
    }
}
