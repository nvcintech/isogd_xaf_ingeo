namespace IsogdXAF.Win.Controllers
{
    partial class IngeoLink
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.SetIngeoLink = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            this.GetIngeoLinkedObject = new DevExpress.ExpressApp.Actions.SimpleAction(this.components);
            // 
            // SetIngeoLink
            // 
            this.SetIngeoLink.Caption = "������� � ���������� ��� ��������";
            this.SetIngeoLink.ConfirmationMessage = null;
            this.SetIngeoLink.Id = "SetIngeoLink";
            this.SetIngeoLink.ToolTip = null;
            this.SetIngeoLink.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.SetIngeoLink_Execute);
            // 
            // GetIngeoLinkedObject
            // 
            this.GetIngeoLinkedObject.ConfirmationMessage = null;
            this.GetIngeoLinkedObject.Id = "GetIngeoLinkedObject";
            this.GetIngeoLinkedObject.TargetObjectsCriteriaMode = DevExpress.ExpressApp.Actions.TargetObjectsCriteriaMode.TrueForAll;
            this.GetIngeoLinkedObject.ToolTip = null;
            this.GetIngeoLinkedObject.Execute += new DevExpress.ExpressApp.Actions.SimpleActionExecuteEventHandler(this.GetIngeoLinkedObject_Execute);

        }

        #endregion

        private DevExpress.ExpressApp.Actions.SimpleAction SetIngeoLink;
        private DevExpress.ExpressApp.Actions.SimpleAction GetIngeoLinkedObject;
    }
}
