using System;
using System.Linq;
using System.Text;
using DevExpress.ExpressApp;
using DevExpress.Data.Filtering;
using System.Collections.Generic;
using DevExpress.Persistent.Base;
using DevExpress.ExpressApp.Utils;
using DevExpress.ExpressApp.Layout;
using DevExpress.ExpressApp.Actions;
using DevExpress.ExpressApp.Editors;
using DevExpress.ExpressApp.Templates;
using DevExpress.Persistent.Validation;
using DevExpress.ExpressApp.SystemModule;
using DevExpress.ExpressApp.Model.NodeGenerators;
using ISOGDXAF.SystemDir;
using DevExpress.Xpo;
using DevExpress.Persistent.BaseImpl;
using Ingeo;
using IsogdXAF.SystemDir.Ingeo;
using DevExpress.XtraEditors;

namespace IsogdXAF.Win.Controllers
{
    // For more typical usage scenarios, be sure to check out http://documentation.devexpress.com/#Xaf/clsDevExpressExpressAppViewControllertopic.
    public partial class IngeoLink : ViewController
    {
        public IngeoLink()
        {
            InitializeComponent();
            RegisterActions(components);
            // Target required Views (via the TargetXXX properties) and create their Actions.
        }
        protected override void OnActivated()
        {
            base.OnActivated();
            // Perform various tasks depending on the target View.
            var currentObject = View.CurrentObject as XPBaseObject;
            if (ObjectSpace.FindObject<SpatialConfig>(CriteriaOperator.Parse(
                    string.Format("IsogdClassName = '{0}'", View.ObjectTypeInfo.Type.FullName))) != null)
            {
                Frame.GetController<IngeoLink>().SetIngeoLink.Active.SetItemValue("myReason", true);
                if (currentObject != null && Connect.FromObjectSpace(ObjectSpace)
                    .IsExist<SpatialRepository>(mc => mc.IsogdObjectId == currentObject.ClassInfo.KeyProperty.GetValue(currentObject)))
                {
                    Frame.GetController<IngeoLink>().GetIngeoLinkedObject.Active.SetItemValue("myReason", true);
                    Frame.GetController<IngeoLink>().GetIngeoLinkedObject.Caption = "�������� �� �����";
                }
                else
                    Frame.GetController<IngeoLink>().GetIngeoLinkedObject.Active.SetItemValue("myReason", false);
            }
            else
            {
                Frame.GetController<IngeoLink>().SetIngeoLink.Active.SetItemValue("myReason", false);
                Frame.GetController<IngeoLink>().GetIngeoLinkedObject.Active.SetItemValue("myReason", false);
            }
        }
        protected override void OnViewControlsCreated()
        {
            base.OnViewControlsCreated();
            // Access and customize the target View control.
        }
        protected override void OnDeactivated()
        {
            // Unsubscribe from previously subscribed events and release other references and resources.
            base.OnDeactivated();
        }
        IngeoSpatialFunctions ingSpatialFunc;

        /// <summary>
        /// ������� ���������� ������ � ����������� �����������������
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SetIngeoLink_Execute(object sender, SimpleActionExecuteEventArgs e)
        {
            // �������� ID � ������������ ������ �������, � �������� ������ �������, � ����� ������
            var id = ((BaseObject)e.CurrentObject).Oid;
            var className = ((BaseObject)e.CurrentObject).ClassInfo.FullName;
            Session session = ((BaseObject)e.CurrentObject).Session;
            UnitOfWork unitofwork = (UnitOfWork)session;

            // �������� �������� �����
            IIngeoApplication ingeo = ingSpatialFunc.GetActiveIngeo();
            if (ingeo == null)
                XtraMessageBox.Show("�� �������� �����.");
            else
            {
                //IList list = ObjectSpace.GetObjects(typeof(SpatialConfig));
                // ���������, ������� ���� ���� ������ �� �����
                if (ingeo.Selection.Count == 0)
                {
                    XtraMessageBox.Show("��� ����������� ������ �� �����");
                }

                // �������� �� ������� ����������� ������� � ��� ���� (� ����� ����� �������� ������� ������ ������ ����)
                var ingeoId = ingeo.Selection.IDs[0];
                object space, number;
                ingeo.ActiveDb.LocalIDToGlobalID(ingeo.ActiveDb.MapObjects.GetObject(ingeoId).LayerID, out space,
                    out number);
                var ingeoLayer = space.ToString() + number;

                var spatial = new SpatialController<GisLayer>(Connect.FromSettings());
                // ��������� � �������, ����� �� ������� ������ � ����
                if (!spatial.IsLayerValid(className, ingeoLayer))
                {
                    XtraMessageBox.Show("������ ����� ������ ������� � ���������� ��� ��������. ��������� ��������� �����."
                        , "������");
                    return;
                }
                // �������� ���������� �������
                var spatialObjects = ingeo.Selection as IEnumerable<object>;

                // �������� �������� - 
                if (!spatial.IsObjectLinked(className.ToString(), id.ToString()))
                {
                    foreach (Dictionary<string, object> spatialObj in spatialObjects)
                    {
                        spatial.AddSpatialLink(className, id.ToString(),
                            spatialObj["LayerId"].ToString(),
                            spatialObj["ObjectId"].ToString());
                    }
                }



                //IIngeoSemDbDataSet ds = dbTable.SelectData("IngeoObjectId", "IngeoObjectId = '" + ingeoId + "'",
                //    null);
                //if (!ds.EOF)
                //{
                //    var xtraMessageBoxForm = new XtraMessageBoxForm();
                //    if (
                //        xtraMessageBoxForm.ShowMessageBoxDialog(new XtraMessageBoxArgs(null,
                //            "������ ������ ��� ������. �����������?",
                //            "������ ������ ��� ������", dialogResults, null, 0)) == DialogResult.Yes)
                //    {
                //        dbTable.DeleteData("IngeoObjectId=?", new object[] { ingeoId });

                //        dbTable.InsertData("ID, IsogdObjectId, IsogdClassId, IngeoObjectId, IngeoLayerId",
                //            "?,?,?,?,?",
                //            new[]
                //            {
                //                ingeo.ActiveDb.GenerateID(), id.ToString(), className, ingeoId, ingeoLayer
                //            });
                //    }
                //    else
                //    {
                //        return;
                //    }
                //}
                //else
                //{
                //    dbTable.InsertData("ID, IsogdObjectId, IsogdClassId, IngeoObjectId, IngeoLayerId",
                //        "?,?,?,?,?",
                //        new[]
                //        {
                //            ingeo.ActiveDb.GenerateID(), id.ToString(), className, ingeoId, ingeoLayer
                //        });
                //}


            }
        }

        private void GetIngeoLinkedObject_Execute(object sender, SimpleActionExecuteEventArgs e)
        {

        }
    }
}
